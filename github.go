package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/go-github/github"
	"golang.org/x/oauth2"
)

// ====== Types =======

// PushHook stores the payload of a github push webhook event
type PushHook struct {
	Ref          string
	Head         string
	Before       string
	After        string
	Size         int
	Created      bool
	Deleted      bool
	Forced       bool
	Compare      string
	Sender       User
	Pusher       User
	Organization Org
	Repository   Repo
	Commits      []Commit
}

// ReleaseHook stores the payload of a github release webhook event
type ReleaseHook struct {
	Action     string
	Release    Release
	Repository Repo
	Sender     User
}

// Release stores the details of a github release
type Release struct {
	ID              int64
	NodeID          string `json:"node_id"`
	TagName         string `json:"tag_name"`
	TargetCommitish string `json:"target_commitish"`
	Name            string
	Draft           bool
	Prerelease      bool
	Author          User
	CreatedAt       string `json:"created_at"`
	PublishedAt     string `json:"published_at"`
	Body            string

	URL        string
	AssetsURL  string `json:"assets_url"`
	UploadURL  string `json:"upload_url"`
	HTMLURL    string `json:"html_url"`
	TarballURL string `json:"tarball_url"`
	ZipballURL string `json:"zipball_url"`
}

// Commit stores info about a single commit
type Commit struct {
	SHA     string `json:"sha"`
	Message string
	URL     string
	Author  User
}

// CommitDetails stores more detailed information about a single commit
type CommitDetails struct {
	SHA       string `json:"sha"`
	NodeID    string `json:"node_id"`
	Author    User
	Committer User
	Commit    CommitMeta
	Files     []File
	Stats     Stats

	URL         string
	HTMLURL     string `json:"html_url"`
	CommentsURL string `json:"comments_url"`
}

// CommitMeta stores information about committer, tree etc
type CommitMeta struct {
	SHA          string `json:"sha"`
	NodeID       string `json:"node_id"`
	Author       User
	Committer    User
	Message      string
	CommentCount int `json:"comment_count"`
	Tree         Commit
	Parents      []Commit
	Verification Verification

	URL     string
	HTMLURL string `json:"html_url"`
}

// Verification stores information about a commit's verification
type Verification struct {
	Verified  bool
	Reason    string
	Signature string
	Payload   string
}

// License stores information about a repo's license
type License struct {
	Key    string
	Name   string
	SPDXID string `json:"spdx_id"`
	URL    string
	NodeID string `json:"node_id"`
}

// Stats stores information about a commit's stats
type Stats struct {
	Additions int
	Deletions int
	Changes   int
	Total     int
}

// File stores information about a file concerned by a commit
type File struct {
	Filename         string
	Additions        int
	Deletions        int
	Changes          int
	SHA              string `json:"sha"`
	Status           string
	PreviousFilename string `json:"previous_filename"`
	Patch            string
	RawURL           string `json:"raw_url"`
	BlobURL          string `json:"blob_url"`
	ContentURL       string `json:"content_url"`
}

// FileResponse stores a github response to a file create/update request
type FileResponse struct {
	Content fileResponseContent
	Commit  CommitMeta
}

type fileResponseContent struct {
	Name        string
	Path        string
	SHA         string
	Size        int
	URL         string
	HTMLURL     string `json:"html_url"`
	GitURL      string `json:"git_url"`
	DownloadURL string `json:"download_url"`
	Type        string
	Links       links `json:"_links"`
}

type links struct {
	Self string
	Git  string
	HTML string
}

// User stores information about a person
type User struct {
	Name       string
	Login      string
	EMail      string
	ID         int64
	NodeID     string `json:"node_id"`
	Type       string
	GravatarID string `json:"gravatar_id"`
	Date       string
	SiteAdmin  bool `json:"site_admin"`

	URL               string
	HTMLURL           string `json:"html_url"`
	AvatarURL         string `json:"avatar_url"`
	FollowersURL      string `json:"followers_url"`
	FollowingURL      string `json:"following_url"`
	GistsURL          string `json:"gists_url"`
	StarredURL        string `json:"starred_url"`
	SubscriptionsURL  string `json:"subscriptions_url"`
	OrganizationsURL  string `json:"organizations_url"`
	ReposURL          string `json:"repos_url"`
	EventsURL         string `json:"events_url"`
	ReceivedEventsURL string `json:"received_events_url"`
}

// Org stores information about a github organization
type Org struct {
	Login            string
	ID               int64
	NodeID           string `json:"node_id"`
	Description      string
	URL              string `json:"url"`
	ReposURL         string `json:"repos_url"`
	EventsURL        string `json:"events_url"`
	HooksURL         string `json:"hooks_url"`
	IssuesURL        string `json:"issues_url"`
	MembersURL       string `json:"memberts_url"`
	PublicMembersURL string `json:"public_members_url"`
	AvatarURL        string `json:"avatar_url"`
}

// Repo stores information about a github repository
type Repo struct {
	ID            int64
	NodeID        string `json:"node_id"`
	Name          string
	FullName      string `json:"full_name"`
	Description   string
	License       License
	Owner         User
	Language      string
	DefaultBranch string `json:"default_branch"`
	MainBranch    string `json:"main_branch"`
	// CreatedAt        int64  `json:"created_at"`
	// UpdatedAt        int64 `json:"updated_at"`
	// PushedAt         string `json:"pushed_at"`
	Size             int
	ForksCount       int `json:"forks_count"`
	Forks            int
	OpenIssuesCount  int `json:"open_issues_count"`
	OpenIssues       int `json:"open_issues"`
	WatchersCount    int `json:"watchers_count"`
	Watchers         int
	StargazersCount  int `json:"stargazers_count"`
	Stargazers       int
	Private          bool
	Form             bool
	Archived         bool
	Disabled         bool
	HasIssues        bool `json:"has_issues"`
	HasProjects      bool `json:"has_projects"`
	HasDownloads     bool `json:"has_downloads"`
	HasWiki          bool `json:"has_wiki"`
	HasPages         bool `json:"has_pages"`
	Homepage         string
	URL              string
	HTMLURL          string `json:"html_url"`
	ForksURL         string `json:"forks_url"`
	KeysURL          string `json:"keys_url"`
	CollaboratorsURL string `json:"collaborators_url"`
	TeamsURL         string `json:"teams_url"`
	HooksURL         string `json:"hooks_url"`
	IssueEventsURL   string `json:"issue_events_url"`
	EventsURL        string `json:"events_url"`
	AssigneesURL     string `json:"assignees_url"`
	BranchesURL      string `json:"branches_url"`
	TagsURL          string `json:"tags_url"`
	BlobsURL         string `json:"blobl_url"`
	GitTagsURL       string `json:"git_tags_url"`
	GitRefsURL       string `json:"git_refs_url"`
	TreesURL         string `json:"trees_url"`
	StatusesURL      string `json:"statuses_url"`
	LanguagesURL     string `json:"languages_url"`
	StargazersURL    string `json:"stargazers_url"`
	ContributorsURL  string `json:"contributors_url"`
	SubscribersURL   string `json:"subscribers_url"`
	SubscriptionURL  string `json:"subscription_url"`
	CommitsURL       string `json:"commits_url"` // we use this
	GitCommitsURL    string `json:"git_commits_url"`
	CommentsURL      string `json:"comments_url"`
	IssueCommentsURL string `json:"issue_comments_url"`
	ContentsURL      string `json:"contents_url"`
	CompareURL       string `json:"compare_url"`
	MergesURL        string `json:"merges_url"`
	ArchiveURL       string `json:"archive_url"`
	DownloadsURL     string `json:"downloads_url"`
	IssuesURL        string `json:"ussues_url"`
	PullsURL         string `json:"pulls_url"`
	MilestonesURL    string `json:"milestones_url"`
	NotificationsURL string `json:"notifications_url"`
	LabelsURL        string `json:"labels_url"`
	ReleasesURL      string `json:"releases_url"`
	DeploymentsURL   string `json:"deployments_url"`
	GitURL           string `json:"git_url"`
	SSHURL           string `json:"ssh_url"`
	CloneURL         string `json:"clone_url"`
	SVNURL           string `json:"svn_url"`
	MirrorURL        string `json:"mirror_url"`
}

// FileInfo stores a file URL, modification type and modification timestamp
// (to be used as value in a map keyed to the filenames)
type FileInfo struct {
	BlobURL   string
	Change    string
	CommitSHA string
	Date      time.Time
	DOIURL    string
	DoPublish bool
	Filename  string
	ObjectSHA string
	RawURL    string
}

// ====== Functions =======

// 1. parse github push request -> get commits, check commit messages for filters
// ( 2. list a commit's files )
// 3. download file from github
// ( changing files, e.g. mixing in DOI, is not done here )
// ( neither is uploading files to zenodo )
// 4. upload a file to github

// ProcessHook processes a Webhook's payload
// Returns a (boolean) doPublish value, a map of filenames/urls with all concerned files, and an error value
func ProcessHook(hookType string, r io.ReadSeeker, conf *Config) (map[string]FileInfo, *Error) {

	switch hookType {
	case "push":
		{
			var payload PushHook

			// Parse payload
			err := json.NewDecoder(r).Decode(&payload)
			if err != nil {
				log.Errorf("Error processing push hook: %+v", err)
				return nil, NewError("errInternal", fmt.Sprintf("error processing push hook: %s", err.Error()), 500, err)
			}

			// Filtering based on repo, pusher or branch
			if conf.Git.Repo != "" && payload.Repository.FullName != conf.Git.Repo {
				log.Warnf("Repo not applicable: %s", payload.Repository.FullName)
				return nil, NewError("warnInapplicable", fmt.Sprintf("repo inapplicable: %s", payload.Repository.FullName), 204, nil)
			}
			if conf.Git.HookUser != "" && payload.Pusher.Name != conf.Git.HookUser {
				log.Warnf("Git pusher not applicable: %s", payload.Pusher.Name)
				return nil, NewError("warnInapplicable", fmt.Sprintf("pusher inapplicable: %s", payload.Pusher.Name), 204, nil)
			}
			if conf.Git.Branch != "" && payload.Ref != "refs/heads/"+conf.Git.Branch {
				log.Warnf("Git push to inapplicable branch: %s instead of %s", payload.Ref, conf.Git.Branch)
				return nil, NewError("warnInapplicable", fmt.Sprintf("branch inapplicable: %s", payload.Ref), 204, nil)
			}

			log.Tracef("Push payload: %+v", payload)
			commitsURL := payload.Repository.CommitsURL

			files := make(map[string]FileInfo)
			// Get all commits and their associated files
			log.Printf("--- Get all commits and their associated files ---")
			for _, c := range payload.Commits {

				commitSHA := c.URL[strings.LastIndex(c.URL, "/")+1:]
				log.Debugf("  commit %s (%s) ...", commitSHA, c.Message)

				// Filtering based on commit messages
				if strings.HasPrefix(c.Message, conf.T2ZCommitMessage) {
					log.Debugf("    Commit %s was created by this service, ignore (avoid recursive calls)", commitSHA)
					continue
				}
				if conf.Git.TriggerPhrase != "" && !(strings.Contains(c.Message, conf.Git.TriggerPhrase)) {
					log.Warnf("  Required trigger phrase not contained in commit message for commit %s", commitSHA)
					continue
				}

				f, err := retrieveFiles(commitsURL, commitSHA, conf)
				if err != nil {
					log.Errorf("Error retrieving files from commit %s: %+v", commitSHA, err)
					return nil, NewError("errGHProcessing", fmt.Sprintf("error retrieving files from commit %s: %s", commitSHA, err), 500, err)
				}

				for k, v := range f {
					if old, ok := files[k]; ok { // file is already present
						if v.Date.After(old.Date) { // but the new entry is newer
							files[k] = v
						} // else (file is already present and newer): do nothing
					} else {
						files[k] = v
					}

				}
			}
			// log.Printf("Parsed hook: %+v", payload)
			if conf.Verbose {
				log.Printf("  Parsed push hook with %d commits and %d files: %+v", len(payload.Commits), len(files), files)
			} else {
				var fls []string
				for s := range files {
					fls = append(fls, s)
				}
				log.Printf("  Parsed push hook with %d commits and %d files: %s", len(payload.Commits), len(files), strings.Join(fls, ", "))
			}
			if len(files) == 0 {
				log.Warnf("No files to process")
				return nil, NewError("warnInapplicable", "no files to process", 204, nil)
			}
			return files, nil
		}
	case "ping":
		{
			log.Printf("Github Ping event received")
			return nil, nil
		}
	default:
		{
			log.Warnf("Unknown hook type %s", hookType)
			return nil, NewError("errWebhook", fmt.Sprintf("hook type %s not implemented", hookType), 501, nil)
		}
	}
}

func retrieveFiles(commitsURL string, commitSHA string, conf *Config) (map[string]FileInfo, error) {
	files := make(map[string]FileInfo)

	// Compile GET request to retrieve commit information
	targetURI := strings.Replace(commitsURL, "{/sha}", "/"+commitSHA, -1)
	req, err := http.NewRequest("GET", targetURI, nil)
	if err != nil {
		log.Errorf("Problem creating GET request: %v ...", err)
		return nil, NewError("errInternal", fmt.Sprintf("error creating GET request: %s", err.Error()), 500, err)
	}
	req.Header.Add("Authorization", "token "+conf.Git.Token)
	req.Header.Add("Accept", "application/vnd.github.v3+json")
	req.Header.Add("User-Agent", "tei2zenodo (https://gitlab.gwdg.de/mpilhlt/tei2zenodo/)")

	// Send GET request
	if conf.Verbose {
		log.Debugf("  Get files at %s:", targetURI)
	} else {
		log.Debugf("  Get files:")
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("Problem sending GET request: %v ...", err)
		return nil, NewError("errNetComm", fmt.Sprintf("error sending GET request: %s", err.Error()), 500, err)
	}
	defer resp.Body.Close()

	// Handle problematic responses
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Problem reading GET response: %v ...", err)
		return nil, NewError("errNetComm", fmt.Sprintf("error reading GET request: %s", err.Error()), 500, err)
	}
	if strconv.Itoa(resp.StatusCode)[:1] != "2" {
		log.Errorf("Problem reported by github: %d %v. %+v ...", resp.StatusCode, err, content)
		return nil, NewError("errGHProcessing", fmt.Sprintf("problem reported by github: %d %v", resp.StatusCode, err), 500, err)
	}

	// Parse response
	var commit CommitDetails
	err = json.Unmarshal(content, &commit)
	if err != nil {
		log.Errorf("Problem parsing github's response: %v ...", err)
		return nil, NewError("errParse", fmt.Sprintf("problem parsing github's response: %s", err), 500, err)
	}

	// Publish or not?
	var doPublish bool
	log.Printf("Commit Message for commit %s: %s", commit.SHA, commit.Commit.Message)
	if conf.Git.DontPublishPhrase != "" && strings.Contains(commit.Commit.Message, conf.Git.DontPublishPhrase) {
		log.Warnf("Dont-publish-Phrase '%s' contained in commit message for commit %s!", conf.Git.DontPublishPhrase, commitSHA)
		doPublish = false
	} else {
		doPublish = true
	}

	// layout := "2006-01-02T15:04:05.000Z"
	str := commit.Commit.Committer.Date
	commitDate, err := time.Parse(time.RFC3339, str)
	if err != nil {
		log.Errorf("Problem parsing commit date: %v", err)
		return nil, NewError("errParseDate", fmt.Sprintf("problem parsing commit date: %s", err), 500, err)
	}

	counter := 0
	for _, f := range commit.Files {
		filename := f.Filename
		rawURL := f.RawURL
		blobURL := f.BlobURL
		objectSHA := f.SHA
		modType := f.Status
		if modType != "deleted" && modType != "renamed" {
			counter++
			if conf.Verbose {
				log.Debugf("    %d. %s '%s' (%s): %s", counter, objectSHA, filename, modType, rawURL)
			} else {
				log.Debugf("    %d. '%s' (%s)", counter, filename, modType)
			}
			if !doPublish {
				log.Warnf("Not going to publish zenodo deposit for %s ! ...", filename)
			}
			files[filename] = FileInfo{Filename: filename, RawURL: rawURL, BlobURL: blobURL, Change: modType, CommitSHA: commitSHA, Date: commitDate, ObjectSHA: objectSHA, DoPublish: doPublish}
		} else {
			log.Debugf("    ignore %s file %s: %s", modType, filename, rawURL)
		}
	}
	return files, nil
}

// DownloadFile takes a Deposit downloads the file from githubRawURL
// Returns a ReadSeeker and an error value
func DownloadFile(myDeposit *Deposit, conf *Config) (r io.ReadSeeker, err *Error) {

	// Compile GET request
	targetURI := myDeposit.GithubRawURL

	log.Debugf("  Download %s ...", targetURI)
	req, RQErr := http.NewRequest("GET", targetURI, nil)
	if RQErr != nil {
		log.Errorf("Problem creating GET request: %v ...", RQErr)
		return nil, NewError("errInternal", fmt.Sprintf("error creating GET request: %s", RQErr.Error()), 500, RQErr)
	}
	req.Header.Add("Authorization", "Bearer "+conf.Git.Token)
	req.Header.Add("User-Agent", "tei2zenodo (https://gitlab.gwdg.de/rg-mpg-de/tei2zenodo/)")

	// Send GET request
	client := &http.Client{}
	resp, RSPErr := client.Do(req)
	if RSPErr != nil {
		log.Errorf("Problem sending GET request: %v ...", RSPErr)
		return nil, NewError("errNetComm", fmt.Sprintf("error sending GET request: %s", RSPErr.Error()), 500, RSPErr)
	}
	defer resp.Body.Close()

	// Read file from Request body
	buf := new(bytes.Buffer)
	length, readErr := buf.ReadFrom(resp.Body)
	if readErr != nil || length == 0 {
		log.Errorf("Problem with empty or invalid response: %v ...", readErr)
		return nil, NewError("Internal", fmt.Sprintf("Problem with empty or invalid response: %v ...", readErr), 500, RSPErr)
	}
	file := buf.Bytes()
	r = bytes.NewReader(file)

	return r, nil
}

// PutFile uploads a file to the github repository and commits
// Returns the commit sha and an error value
func PutFile(myDeposit *Deposit, conf *Config, c *gin.Context) (string, error) {

	githubConf := conf.Git

	log.Printf("--- Upload new version to github ---")
	log.Printf("  Put file %s to github ...", myDeposit.Filename)
	// log.Debugf("    Upload file: %s ...", myDeposit.FileContent[:80])

	// authenticate with Personal Access Token/Oauth2
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: githubConf.Token},
	)
	tc := oauth2.NewClient(c, ts)
	client := github.NewClient(tc)

	// 1. remember old commit SHA (myDeposit.CommitSHA)
	ownerName := strings.Split(githubConf.Repo, "/")[0]
	repoName := strings.Split(githubConf.Repo, "/")[1]
	branch := githubConf.Branch
	ref, _, GRErr := client.Git.GetRef(c, ownerName, repoName, "refs/heads/"+branch)
	if GRErr != nil {
		log.Errorf("Problem getting branch reference: %v ...", GRErr)
		return "", NewError("errGHProcessing", fmt.Sprintf("problem getting branch reference: %s", GRErr.Error()), 500, GRErr)
	}
	oldCommitSHA := ref.Object.SHA
	log.Tracef("    Commit to base the new commit on (head of branch %s): %v", branch, oldCommitSHA)

	// 2. get old commit and its tree (=baseTree)
	oldCommit, _, OCErr := client.Git.GetCommit(c, ownerName, repoName, *oldCommitSHA)
	if OCErr != nil {
		log.Errorf("Problem getting old commit: %v ...", OCErr)
		return "", NewError("errGHProcessing", fmt.Sprintf("problem getting old commit: %s", OCErr.Error()), 500, OCErr)
	}
	baseTreeSHA := oldCommit.SHA
	baseTree, _, GTErr := client.Git.GetTree(c, ownerName, repoName, *baseTreeSHA, true)
	if GTErr != nil {
		log.Errorf("Problem getting old tree: %v ...", GTErr)
		return "", NewError("errGHProcessing", fmt.Sprintf("problem getting old tree: %s", GTErr.Error()), 500, GTErr)
	}
	if *baseTree.Truncated {
		log.Error("Old tree is too large and has been truncated. This is not supported yet")
		return "", NewError("errGHProcessing", "old tree is too large and has been truncated. This is not supported yet", 500, nil)
	}
	log.Tracef("    Tree object of the old commit: %v", *baseTree)

	// 3. create and post new tree object (get new treeSHA)
	newEntry := github.TreeEntry{
		Path:    github.String(myDeposit.Filename),
		Mode:    github.String("100644"),
		Type:    github.String("blob"),
		Content: github.String(myDeposit.FileContent),
		//	SHA:     blob.SHA,
		//	Size    *int    `json:"size,omitempty"`
		//	URL     *string `json:"url,omitempty"`
	}
	entries := []github.TreeEntry{}
	entries = append(entries, newEntry)
	newTree, _, NTErr := client.Git.CreateTree(c, ownerName, repoName, *baseTree.SHA, entries)
	if NTErr != nil {
		log.Errorf("Problem creating new tree: %v ...", NTErr)
		return "", NewError("errGHProcessing", fmt.Sprintf("problem creating new tree: %s", NTErr.Error()), 500, NTErr)
	}
	log.Tracef("    New tree object: %v", *newTree)

	// 4. create and post new commit object (use old commit SHA as parent and new tree SHA; get new commit SHA)
	var commitMessage string
	if myDeposit.DoPublish {
		commitMessage = conf.T2ZCommitMessage + " in " + myDeposit.Filename + "." // "Zenodo DOI updated"
	} else {
		commitMessage = conf.T2ZCommitMessage + " in " + myDeposit.Filename + " (not published at zenodo yet)."
	}
	// commitAuthor := "tei2zenodo service"
	// commitAuthorEMail := "bla@foo.com"
	// date := time.Now()
	// author := &github.CommitAuthor{Date: &date, Name: &commitAuthor, Email: &commitAuthorEMail}
	// commit := &github.Commit{Author: author, Message: &commitMessage, Tree: newTree, Parents: []*github.Commit{oldCommit}}
	commit := &github.Commit{Message: &commitMessage, Tree: newTree, Parents: []github.Commit{*oldCommit}}
	newCommit, _, NCErr := client.Git.CreateCommit(c, ownerName, repoName, commit)
	if NCErr != nil {
		log.Errorf("error creating commit object: %v ...", NCErr)
		return "", NewError("errGHProcessing", fmt.Sprintf("error creating commit object: %s", NCErr.Error()), 500, NCErr)
	}
	log.Tracef("    New commit object: %v", *newCommit)

	// 5. update heads/{branch} to point to new commit SHA
	ref.Object.SHA = newCommit.SHA
	_, _, URErr := client.Git.UpdateRef(c, ownerName, repoName, ref, false)
	if URErr != nil {
		log.Errorf("Problem updating branch reference: %v ...", URErr)
		return "", NewError("errGHProcessing", fmt.Sprintf("problem updating branch reference: %s", URErr.Error()), 500, URErr)
	}
	log.Tracef("    Updated ref object: %v", *ref)

	myDeposit.CommitSHA = *newCommit.SHA

	// 6. Done, reporting...
	// log.Printf("Response code %d, content: %s", resp.StatusCode, content)
	log.Printf("    Success. Commit SHA: %s", myDeposit.CommitSHA)

	return myDeposit.CommitSHA, nil

	// For testing: uri := "https://postman-echo.com/post"
	// https://api.github.com/repos/:owner/:repo/contents/:path
	// uri := githubConf.Host + "/repos/" + githubConf.Repo + "/contents/" + myDeposit.Filename // ?access_token=" + conf.Token
	// log.Printf("  Put request to: %s", uri)

	// Compile upload form and prepare PUT request
	// parameters:
	// message	string	Required. The commit message.
	// content	string	Required. The new file content, using Base64 encoding.
	// sha		string	Required if you are updating a file. The blob SHA of the file being replaced.
}
