package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"
)

// ====== Types ======

// Deposit stores a zenodo Deposit, part of it is returned by this service
type Deposit struct {
	Changed       bool
	CommitSHA     string
	ConceptDOI    string `json:"conceptdoi"`
	ConceptRecID  string `json:"conceptrecid"`
	Created       string
	DepositDOI    string `json:"doi"`
	DOIURL        string `json:"doi_url"`
	DoPublish     bool
	FileContent   string
	Filename      string
	Files         []ZFile
	Filesize      int64
	GithubBlobURL string
	GithubObjSHA  string
	GithubRawURL  string
	ID            int64
	Links         ZLinks
	Metadata      ZMetadata `json:"metadata"`
	Modified      string
	OldDOI        string
	Owner         int64
	RecordID      int64 `json:"record_id"`
	State         string
	Submitted     bool
	Title         string
	ZenodoURL     string
}

// ZFile stores a zenodo file response
type ZFile struct {
	Checksum string
	Filename string
	Filesize float64
	ID       string
	Links    ZLinks
}

// ZLinks holds links to data facets and actions
type ZLinks struct {
	DiscardURI            string `json:"discard"`
	DownloadURI           string `json:"download"`
	EditURI               string `json:"edit"`
	FilesURI              string `json:"files"`
	LatestDraftURI        string `json:"latest_draft"`
	NewVersionURI         string `json:"newversion"`
	PublishURI            string `json:"publish"`
	RegisterConceptDOIURI string `json:"registerconceptdoi"`
	SelfURI               string `json:"self"`
}

// ZMetadata holds metadata that zenodo uses (based on DataCite's metadata scheme)
type ZMetadata struct {
	UploadType string `json:"upload_type"` // mandatory
	// values: 	publication, poster, presentation, dataset, image, video, software,
	// 			lesson, other
	PublicationType string `json:"publication_type"` // mandatory if upload_type=publication
	// values: 	annotationcollection, book, section, conferencepaper, datamanagementplan,
	// 			article, patent, preprint, deliverable, milestone, proposal, report,
	// 			softwaredocumentation, taxonomictreatment, technicalnote, thesis,
	// 			workingpaper, other
	PublicationDate string     `json:"publication_date"` // mandatory, in ISO8601 format (YYYY-MM-DD)
	Title           string     `json:"title"`            // mandatory
	Creators        []ZCreator `json:"creators"`         // mandatory
	Description     string     `json:"description"`      // mandatory, can contain html
	AccessRight     string     `json:"access_right"`     // mandatory
	// values: 	open, embargoed, restricted, closed
	License string `json:"license,omitempty"` // mandatory if access_right=(open|embargoed)
	// values: 	 See Open Definition Licenses Service.
	// Defaults to cc-by for non-datasets and cc-zero for datasets
	EmbargoDate           string         `json:"embargo_date,omitempty"`            // mandatory if access_right=embargoed, in ISO8601 format (YYYY-MM-DD)
	AccessConditions      string         `json:"access_conditions,omitempty"`       // mandatory if access_right=restricted, can contain html
	Contributors          []ZContributor `json:"contributors,omitempty"`            // optional
	DOI                   string         `json:"doi"`                               // optional
	Keywords              []string       `json:"keywords,omitempty"`                // optional, free form
	Notes                 string         `json:"notes,omitempty"`                   // optional, can contain html
	RelatedIdentifiers    []ZIdentifier  `json:"related_identifiers,omitempty"`     // optional
	References            []string       `json:"references,omitempty"`              // optional
	Communities           []ZCommunity   `json:"communities,omitempty"`             // optional
	Grants                []ZGrant       `json:"grants,omitempty"`                  // optional
	JournalTitle          string         `json:"journal_title,omitempty"`           // optional
	JournalVolume         string         `json:"journal_volume,omitempty"`          // optional
	JournalIssue          string         `json:"journal_issue,omitempty"`           // optional
	JournalPages          string         `json:"journal_pages,omitempty"`           // optional
	ConferenceTitle       string         `json:"conference_title,omitempty"`        // optional
	ConferenceAcronym     string         `json:"conference_acronym,omitempty"`      // optional
	ConferenceDates       string         `json:"conference_dates,omitempty"`        // optional
	ConferencePlace       string         `json:"conference_place,omitempty"`        // optional
	ConferenceURL         string         `json:"conference_url,omitempty"`          // optional
	ConferenceSession     string         `json:"conference_session,omitempty"`      // optional
	ConferenceSessionPart string         `json:"conference_session_part,omitempty"` // optional
	ImprintPublisher      string         `json:"imprint_publisher,omitempty"`       // optional
	ImprintISBN           string         `json:"imprint_isbn,omitempty"`            // optional
	ImprintPlace          string         `json:"imprint_place,omitempty"`           // optional
	PartofTitle           string         `json:"partof_title,omitempty"`            // optional
	PartofPages           string         `json:"partof_pages,omitempty"`            // optional
	ThesisSupervisors     []ZCreator     `json:"thesis_supervisors,omitempty"`      // optional
	ThesisUniversity      string         `json:"thesis_university,omitempty"`       // optional
	Subjects              []ZSubject     `json:"subjects,omitempty"`                // optional
	Version               string         `json:"version,omitempty"`                 // optional
	Language              string         `json:"language,omitempty"`                // optional, ISO 639-2 or 639-3 code
	Locations             []ZLocation    `json:"locations,omitempty"`               // optional
	Dates                 []ZDate        `json:"dates,omitempty"`                   // optional
	Method                string         `json:"method,omitempty"`                  // optional, can contain html
	//PrereserveDOI      *ZPrereserveMetadata `json:"prereserve_doi,omitempty"` // optional, use this to reserve and get a doi to include in your upload
}

// ZPrereserveMetadata stores DOI prereservation metadata
type ZPrereserveMetadata struct {
	PrereserveDOI ZPrereserveDOI `json:"prereserve_doi"`
}

// ZPrereserveDOI stores prereservation information
type ZPrereserveDOI struct {
	DOI   string
	RecID int64
}

// ZCreator holds agents who have created a deposit
type ZCreator struct {
	Name        string `json:"name"`                  // mandatory; family name, given names
	Affiliation string `json:"affiliation,omitempty"` // optional
	ORCID       string `json:"orcid,omitempty"`       // optional
	GND         string `json:"gnd,omitempty"`         // optional
}

// ZContributor holds agents who have contributed to creating a deposit
type ZContributor struct {
	Name string `json:"name"` // mandatory; family name, given names
	Type string `json:"type"` // mandatory
	// values: 	ContactPerson, DataCollector, DataCurator, DataManager,
	// 			Distributor, Editor, Funder, HostingInstitution,
	// 			Producer, ProjectLeader, ProjectManager, ProjectMember,
	// 			RegistrationAgency, RegistrationAuthority, RelatedPerson,
	// 			Researcher, ResearchGroup, RightsHolder, Supervisor,
	// 			Sponsor, WorkPackageLeader, Other
	Affiliation string `json:"affiliation,omitempty"` // optional
	ORCID       string `json:"orcid,omitempty"`       // optional
	GND         string `json:"gnd,omitempty"`         // optional
}

// ZCommunity holds zenodo's communities of interest
type ZCommunity struct {
	Identifier string `json:"identifier"`
}

// ZGrant holds grant ids
type ZGrant struct {
	ID string `json:"id"` // funder DOI-prefixed grant ids
}

// ZIdentifier holds identifiers of resources related to the deposit
type ZIdentifier struct {
	Relation string `json:"relation"` // mandatory
	// values: 	isCitedBy, cites, isSupplementTo, isSupplementedBy,
	// 			isNewVersionOf, isPreviousVersionOf, isPartOf, hasPart,
	// 			compiles, isCompiledBy, isIdenticalTo, isAlternateIdentifier
	Identifier string `json:"identifier"` // mandatory
}

// ZSubject holds what the deposit is about (controlled in an external scheme)
type ZSubject struct {
	Term       string `json:"term,omitempty"`
	Identifier string `json:"identifier,omitempty"`
	Scheme     string `json:"scheme,omitempty"`
}

// ZLocation holds places relevant to the deposit
type ZLocation struct {
	Place       string  `json:"place"`                 // mandatory
	Description string  `json:"description,omitempty"` // optional
	Lat         float64 `json:"lat,omitempty"`
	Lon         float64 `json:"lon,omitempty"`
}

// ZDate holds dates relevant to the deposit
type ZDate struct {
	Start       string `json:"start,omitempty"`       // one of start or end must be present, in ISO8601 format (YYYY-MM-DD)
	End         string `json:"end,omitempty"`         // one of start or end must be present, in ISO8601 format (YYYY-MM-DD)
	Type        string `json:"type"`                  // mandatory, values: 	Collected, Valid, Withdrawn
	Description string `json:"description,omitempty"` // optional
}

// DOIResponse stores a response from doi.org
type DOIResponse struct {
	ResponseCode int
	Handle       string
	Values       []DOIValue
}

// DOIValue stores a single value of a doi response
type DOIValue struct {
	Index     int
	Type      string
	TTL       int64
	Timestamp string
	Data      DOIData
}

// DOIData stores data of doi response values
type DOIData struct {
	Format string
	Value  string
}

// ZPostData wraps the metadata in a "metadata" object
type ZPostData struct {
	Metadata *ZMetadata `json:"metadata"`
}

// ====== Functions ======

// ProcessFile takes a file and processes parsing and zenodo upload
func ProcessFile(r io.ReadSeeker, doi string, md *ZMetadata, myDeposit *Deposit, conf *Config) *Error {

	doPublish := myDeposit.DoPublish

	if doi == "" {
		log.Printf("--- Create new DOI at zenodo ---")
		doi, DOIErr := createNewDOI(md, conf)
		if DOIErr != nil || doi == "" {
			log.Errorf("Error creating DOI reservation deposit: %v", DOIErr)
			return NewError("errZProcessing", fmt.Sprintf("error creating DOI reservation deposit: %s", DOIErr), 500, DOIErr)
		}
		myDeposit.DepositDOI = doi
		myDeposit.Metadata.DOI = doi
		md.DOI = doi
	} else {
		log.Printf("--- Retrieve deposit for %s and create a new version ---", doi)
		myDeposit.OldDOI = doi
		md.RelatedIdentifiers = append(md.RelatedIdentifiers, ZIdentifier{Relation: "isNewVersionOf", Identifier: doi})
		newDOI, DOIErr := updateDeposit(myDeposit, md, conf)
		if DOIErr != nil {
			log.Errorf("Error retrieving zenodo deposit for doi %s: %v", doi, DOIErr)
			return NewError("errZProcessing", fmt.Sprintf("error retrieving zenodo deposit for %s: %s", doi, DOIErr), 500, DOIErr)
		}
		myDeposit.DepositDOI = newDOI
		md.DOI = newDOI
	}

	if conf.WriteDOI.Mode == "add" || conf.WriteDOI.Mode == "replace" {
		log.Printf("--- Add new DOI to document (mode: %s) ---", conf.WriteDOI.Mode)
		newfile, MXErr := MixinDOI(r, md.DOI, conf)
		if MXErr != nil {
			log.Errorf("Error mixing new DOI into document: %v", MXErr)
			return NewError("errInternal", fmt.Sprintf("error mixing new DOI into document: %s", MXErr), 500, MXErr)
		}
		myDeposit.FileContent = newfile
		myDeposit.Changed = true
		r = bytes.NewReader([]byte(newfile))
		_, _ = r.Seek(0, 0)
	} else {
		log.Printf("--- Skip adding new DOI to document ---")
		buf := new(strings.Builder)
		_, err := io.Copy(buf, r)
		if err != nil {
			log.Errorf("Could not read file.")
			return NewError("errParse", "could not read file", 500, err)
		}
		myDeposit.FileContent = buf.String()
		_, _ = r.Seek(0, 0)
	}

	log.Printf("--- Upload to zenodo ---")
	uploadFilename := strings.Replace(myDeposit.Filename, "/", "_", -1) // We replace the / (SOLIDUS, U+002F) with ∕ (DIVISION SLASH, U+2215)
	url, PFErr := postFile(r, uploadFilename, md, conf)
	if PFErr != nil {
		log.Errorf("Error uploading to zenodo, sending POST request: %v", PFErr)
		return NewError("errZProcessing", fmt.Sprintf("error uploading to zenodo, sending POST request: %s", PFErr), 500, PFErr)
	}
	myDeposit.ZenodoURL = url
	myDeposit.DOIURL = "https://doi.org/" + md.DOI

	log.Printf("--- Add metadata to zenodo ---")
	PMErr := putMetadata(myDeposit, md, conf)
	if PMErr != nil {
		log.Errorf("Error putting metadata: %v", PMErr)
		return NewError("errZProcessing", fmt.Sprintf("error putting metadata to zenodo: %s", PMErr), 500, PMErr)
	}

	if doPublish {
		log.Printf("--- Publish at zenodo ---")
		PErr := publish(md, conf)
		if PErr != nil {
			log.Errorf("Error publishing deposit: %v", PErr)
			return NewError("errZProcessing", fmt.Sprintf("error publishing deposit: %s", PErr), 500, PErr)
		}
	} else {
		log.Printf("")
		log.Printf("--- ---")
		log.Warnf("!!! Not published!")
		log.Printf("!!! To publish this deposit, visit zenodo's website,")
		log.Printf("!!! log in and click on the 'Upload' button.")
		log.Printf("!!! If you want these commits to be published in general, either")
		log.Printf("!!! - set doPublish=True as request parameter or")
		log.Printf("!!! - remove the 'commit_dontpublish_phrase' from")
		log.Printf("!!!   either the tei2zenodo service config or your commit messages.")
	}
	return nil
}

// createDeposit creates an empty zenodo deposit and reserves a DOI
// it returns the DOI of the new deposit and an error value
func createDeposit(targetURI string, md *ZMetadata, c *Config) (string, error) {

	conf := c.Zenodo
	// Compile POST request
	if c.Verbose {
		log.Debugf("  Post request to: %s", targetURI)
	} else {
		log.Debugf("  Create new deposit")
	}

	body := ZPostData{Metadata: md}
	mdJSON, err := json.Marshal(body)
	if err != nil {
		log.Errorf("Problem converting metadata to JSON format: %v ...", err)
		return "", fmt.Errorf("problem converting metadata to JSON format: %s", err)
	}

	var buf *bytes.Buffer
	if targetURI[len(targetURI)-19:] == "/actions/newversion" {
		buf = bytes.NewBuffer(mdJSON)
	} else {
		buf = bytes.NewBuffer([]byte(`{}`))
	}
	req, err := http.NewRequest("POST", targetURI, buf)
	if err != nil {
		log.Errorf("Problem creating POST request: %v ...", err)
		return "", fmt.Errorf("problem creating POST request: %s", err)
	}
	req.Header.Add("Content-Type", `application/json`)
	req.Header.Add("Authorization", "Bearer "+conf.Token)
	req.Header.Add("User-Agent", "tei2zenodo (https://gitlab.gwdg.de/mpilhlt/tei2zenodo/)")

	// Send POST request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("Problem sending POST request: %v ...", err)
		return "", fmt.Errorf("problem sending POST request: %s", err)
	}
	defer resp.Body.Close()

	// Handle problematic responses
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Problem reading POST response: %v ...", err)
		return "", fmt.Errorf("problem reading POST response: %s", err)
	}
	if strconv.Itoa(resp.StatusCode)[:1] != "2" {
		log.Errorf("Problem reported by zenodo: %d %v. %s ...", resp.StatusCode, err, content)
		return "", fmt.Errorf("problem reported by zenodo: %d %v. %s", resp.StatusCode, err, content)
	}

	// Parse response
	var parsedContent Deposit
	err = json.Unmarshal(content, &parsedContent)
	if err != nil {
		log.Errorf("Problem parsing zenodo's response: %v ...", err)
		return "", fmt.Errorf("problem parsing zenodo's response: %s", err)
	}

	var doi string
	if targetURI[len(targetURI)-19:] == "/actions/newversion" {
		i := parsedContent.Links.LatestDraftURI
		id := i[strings.LastIndex(i, "/")+1:]
		doi = conf.Prefix + id
	} else {
		doi = conf.Prefix + strconv.Itoa(int(parsedContent.ID))
	}

	if c.Verbose {
		log.Debugf("    Success (code %d (expected 201)). DOI %s is created and %s.", resp.StatusCode, doi, parsedContent.State)
	} else {
		log.Debugf("    Success. New DOI: %s", doi)
	}
	return doi, nil
}

// createNewDOI creates a Deposit from Scratch
// it returns a new zenodo DOI for the deposit and an error value
func createNewDOI(md *ZMetadata, c *Config) (string, error) {
	conf := c.Zenodo
	targetURI := conf.Host + "/api/deposit/depositions"
	doi, err := createDeposit(targetURI, md, c)
	if err != nil {
		log.Errorf("Problem creating new DOI/deposit: %v ...", err)
		return "", fmt.Errorf("problem creating new DOI/deposit: %s", err)
	}
	return doi, nil
}

// updateDeposit retrieves a zenodo deposit based on a gived DOI and creates a new version
// it returns a new zenodo DOI for the deposit and an error value
func updateDeposit(d *Deposit, md *ZMetadata, c *Config) (string, error) {
	conf := c.Zenodo
	if d.OldDOI[:len(conf.Prefix)] != conf.Prefix {
		log.Errorf("Problem: DOI %s is not a zenodo DOI.", d.OldDOI)
		return "", fmt.Errorf("invalid DOI value")
	}
	oldDOI := d.OldDOI[len(conf.Prefix):]

	// Compile GET request
	targetURI := conf.Host + "/api/deposit/depositions/" + oldDOI
	req, err := http.NewRequest("GET", targetURI, nil)
	if err != nil {
		log.Errorf("Problem creating GET request in deposit update: %v ...", err)
		return "", fmt.Errorf("problem creating GET request in deposit update: %s", err)
	}
	req.Header.Add("Authorization", "Bearer "+conf.Token)
	req.Header.Add("User-Agent", "tei2zenodo (https://gitlab.gwdg.de/rg-mpg-de/tei2zenodo/)")

	// Send GET request
	if c.Verbose {
		log.Debugf("  Get deposit for %s from %s", oldDOI, targetURI)
	} else {
		log.Debugf("  Get deposit for %s", oldDOI)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("Problem creating GET request: %v ...", err)
		return "", fmt.Errorf("problem creating GET request: %s", err)
	}
	defer resp.Body.Close()

	// Handle problematic responses
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Problem reading GET response: %v ...", err)
		return "", fmt.Errorf("problem reading GET response: %s", err)
	}
	switch strconv.Itoa(resp.StatusCode)[:1] {
	case "4", "5":
		{
			if resp.StatusCode == http.StatusGone { // 410 Gone
				log.Warnf("PID %s has been deleted, create new one from scratch", d.OldDOI)
				return createNewDOI(md, c)
			}
			log.Errorf("Problem reported by resolver: %d %v. %s ...", resp.StatusCode, err, content)
			return "", fmt.Errorf("problem reported by resolver: %d %v. %s", resp.StatusCode, err, content)
		}
	}

	// Parse response
	err = json.Unmarshal(content, d)
	if err != nil {
		log.Errorf("Problem parsing resolver's response: %v ...", err)
		return "", fmt.Errorf("problem parsing resolver's response: %s", err)
	}
	d.OldDOI = oldDOI
	log.Debugf("    Success.")

	newVersionLink := d.Links.NewVersionURI
	if c.Verbose {
		log.Debugf("  Now get new version at %s", newVersionLink)
	}
	newDOI, err := createDeposit(newVersionLink, md, c)
	if err != nil {
		log.Errorf("Problem creating new version/deposit: %v ...", err)
		return "", fmt.Errorf("problem creating new version/deposit: %s", err)
	}
	md.DOI = newDOI

	return newDOI, nil
}

// getFiles gets all files associated to a deposit
// it returns a slice of ZFile objects
func getFiles(doi string, c *Config) ([]ZFile, error) {
	conf := c.Zenodo

	if doi[:len(conf.Prefix)] != conf.Prefix {
		log.Errorf("Problem: DOI %s is not a zenodo DOI.", doi)
		return nil, fmt.Errorf("invalid DOI value")
	}
	id := doi[len(conf.Prefix):]

	// Compile GET request
	targetURI := conf.Host + "/api/deposit/depositions/" + id + "/files"
	req, err := http.NewRequest("GET", targetURI, nil)
	if err != nil {
		log.Errorf("Problem creating GET request: %v ...", err)
		return nil, fmt.Errorf("problem creating GET request: %s", err)
	}
	req.Header.Add("Authorization", "Bearer "+conf.Token)
	req.Header.Add("User-Agent", "tei2zenodo (https://gitlab.gwdg.de/rg-mpg-de/tei2zenodo/)")

	// Send GET request
	if c.Verbose {
		log.Debugf("    Get files for %s from: %s", doi, targetURI)
	} else {
		log.Debugf("    Get files for %s", doi)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("Problem creating GET request: %v ...", err)
		return nil, fmt.Errorf("problem creating GET request: %s", err)
	}
	defer resp.Body.Close()

	// Handle problematic responses
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Problem reading GET response: %v ...", err)
		return nil, fmt.Errorf("problem reading GET response: %s", err)
	}
	if strconv.Itoa(resp.StatusCode)[:1] != "2" {
		log.Errorf("Problem reported by zenodo: %d %v. %s ...", resp.StatusCode, err, content)
		return nil, fmt.Errorf("problem reported by zenodo: %d %v. %s", resp.StatusCode, err, content)
	}

	// Parse response
	var files []ZFile
	err = json.Unmarshal(content, &files)
	if err != nil {
		log.Errorf("Problem parsing resolver's response: %v ...", err)
		return nil, fmt.Errorf("problem parsing resolver's response: %s", err)
	}

	if c.Verbose {
		log.Debugf("    Files: %+v", files)
	}

	return files, nil
}

// deleteFileByID deletes a file from a zenodo deposit by its ID
// it returns an error value
func deleteFileByID(doi string, id string, c *Config) error {
	conf := c.Zenodo

	if doi[:len(conf.Prefix)] != conf.Prefix {
		log.Errorf("Problem: DOI %s is not a zenodo DOI.", doi)
		return fmt.Errorf("invalid DOI value")
	}
	dID := doi[len(conf.Prefix):]

	// Compile DELETE request
	targetURI := conf.Host + "/api/deposit/depositions/" + dID + "/files/" + id
	req, err := http.NewRequest("DELETE", targetURI, nil)
	if err != nil {
		log.Errorf("Problem creating DELETE request: %v ...", err)
		return fmt.Errorf("problem creating DELETE request: %s", err)
	}
	req.Header.Add("Authorization", "Bearer "+conf.Token)
	req.Header.Add("User-Agent", "tei2zenodo (https://gitlab.gwdg.de/rg-mpg-de/tei2zenodo/)")

	// Send DELETE request
	if c.Verbose {
		log.Debugf("  Delete file %s from deposit %s", id, doi)
	} else {
		log.Debugf("  Delete file %s", id)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("Problem sending DELETE request: %v ...", err)
		return fmt.Errorf("problem sending DELETE request: %s", err)
	}
	defer resp.Body.Close()

	// Handle problematic responses
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Problem reading DELETE response: %v ...", err)
		return fmt.Errorf("problem reading DELETE response: %s", err)
	}
	if strconv.Itoa(resp.StatusCode)[:1] != "2" {
		log.Errorf("Problem reported by zenodo: %d %v. %s ...", resp.StatusCode, err, content)
		return fmt.Errorf("problem reported by zenodo: %d %v. %s", resp.StatusCode, err, content)
	}
	return nil
}

// mustDeleteFileByName deletes a file from a zenodo deposit by its name
// it returns an error value and fails when the file is not present
func mustDeleteFileByName(doi string, filename string, c *Config) error {
	conf := c.Zenodo

	if doi[:len(conf.Prefix)] != conf.Prefix {
		log.Errorf("Problem: DOI %s is not a zenodo DOI.", doi)
		return fmt.Errorf("invalid DOI value")
	}

	files, err := getFiles(doi, c)
	if err != nil {
		log.Errorf("Problem: Could not get files for DOI %s.", doi)
		return fmt.Errorf("could not get files for %s", doi)
	}
	for _, f := range files {
		if c.Verbose {
			log.Tracef("test %s...", f.Filename)
		}
		if f.Filename == filename {
			err = deleteFileByID(doi, f.ID, c)
			if err != nil {
				log.Errorf("Problem: Could not delete file %s.", f.ID)
				return fmt.Errorf("could not delete file %s", f.ID)
			}
			log.Debugf("    Success. Deleted file %s.", filename)
			return nil
		}
	}
	return fmt.Errorf("no file with name %s found in deposit %s", filename, doi)
}

// deleteFileByName deletes a file from a zenodo deposit by its name.
// it does not raise an error if the file does not exist in the first place.
// it returns an error value
func deleteFileByName(doi string, filename string, c *Config) error {
	conf := c.Zenodo
	if doi[:len(conf.Prefix)] != conf.Prefix {
		log.Errorf("Problem: DOI %s is not a zenodo DOI.", doi)
		return fmt.Errorf("invalid DOI value")
	}

	files, err := getFiles(doi, c)
	if err != nil {
		log.Errorf("Problem: Could not get files for DOI %s.", doi)
		return fmt.Errorf("could not get files for %s", doi)
	}
	for _, f := range files {
		if f.Filename == filename {
			err = deleteFileByID(doi, f.ID, c)
			if err != nil {
				log.Errorf("Problem: Could not delete file %s.", f.ID)
				return fmt.Errorf("could not delete file %s", f.ID)
			}
			log.Debugf("    Success: Deleted file %s.", filename)
			return nil
		}
	}
	log.Debugf("    No file %s to delete.", filename)
	return nil
}

// postFile posts a file to zenodo, taking the id from the md.DOI field
// it returns an URI for the upload and an error value
func postFile(r io.Reader, filename string, md *ZMetadata, c *Config) (string, error) {
	conf := c.Zenodo

	if md.DOI[:len(conf.Prefix)] != conf.Prefix {
		log.Errorf("Problem: DOI %s is not a zenodo DOI.", md.DOI)
		return "", fmt.Errorf("invalid DOI value")
	}
	id := md.DOI[len(conf.Prefix):]

	log.Debugf("  Make sure no file named %s exists in doi %s.", filename, md.DOI)
	err := deleteFileByName(md.DOI, filename, c)
	if err != nil {
		log.Errorf("Problem deleting file %s: %v", filename, err)
		return "", fmt.Errorf("problem deleting file %s: %s", filename, err)
	}

	// Read file from Request body
	buf := new(bytes.Buffer)
	length, err := buf.ReadFrom(r)
	if err != nil {
		log.Errorf("Problem reading from reader: %v", err)
		return "", fmt.Errorf("problem reading from reader: %s", err)
	}
	if length == 0 {
		log.Errorf("Cannot upload empty file: %s", filename)
		return "", fmt.Errorf("cannot upload empty file")
	}
	file := buf.String()
	reader := strings.NewReader(file)

	// For testing: uri := "https://postman-echo.com/post"
	uri := conf.Host + "/api/deposit/depositions/" + id + "/files" // ?access_token=" + conf.Token
	if c.Verbose {
		log.Printf("  Post request to: %s", uri)
		log.Debugf("    Upload filename: %s", filename)
		log.Debugf("    Upload file: %s ...", file[:80])
	} else {
		log.Printf("  Post file %s to zenodo", filename)
	}

	// Compile upload form and prepare POST request
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)

	label, err := writer.CreateFormField("name")
	if err != nil {
		log.Errorf("Problem creating POST request body: %v ...", err)
		return "", fmt.Errorf("problem creating POST request body: %s", err)
	}
	_, err = label.Write([]byte(filename))
	if err != nil {
		log.Errorf("Problem creating form field: %v ...", err)
		return "", fmt.Errorf("problem creating form field: %s", err)
	}

	part, err := writer.CreateFormFile(`file`, filename)
	if err != nil {
		log.Errorf("Problem creating POST request body: %v ...", err)
		return "", fmt.Errorf("problem creating POST request body: %s", err)
	}
	length, err = io.Copy(part, reader)
	if err != nil {
		log.Errorf("Problem copying file data: %v ...", err)
		return "", fmt.Errorf("problem copying file data: %s", err)
	}
	if length == 0 {
		log.Errorf("Cannot upload empty file: %s", filename)
		return "", fmt.Errorf("cannot upload empty file")
	}
	writer.Close()

	req, err := http.NewRequest("POST", uri, body)
	if err != nil {
		log.Errorf("Problem creating POST request: %v ...", err)
		return "", fmt.Errorf("problem creating POST request: %s", err)
	}
	req.Header.Add(`Content-Type`, writer.FormDataContentType())
	req.Header.Add("Authorization", "Bearer "+conf.Token)
	req.Header.Add("User-Agent", "tei2zenodo (https://gitlab.gwdg.de/rg-mpg-de/tei2zenodo/)")

	_, _ = reader.Seek(0, 0)

	// Send POST request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("Problem sending POST request: %v ...", err)
		return "", fmt.Errorf("problem sending POST request: %s", err)
	}
	defer resp.Body.Close()

	// Handle problematic responses
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Problem reading POST response: %v ...", err)
		return "", fmt.Errorf("problem reading POST response: %s", err)
	}
	if strconv.Itoa(resp.StatusCode)[:1] != "2" {
		log.Errorf("Problem reported by zenodo: %d %v. %s ...", resp.StatusCode, err, content)
		return "", fmt.Errorf("problem reported by zenodo: %v. %s", err, content)
	}

	// Parse response to extract filesize and download URI
	// log.Printf("Response code %d, content: %s", resp.StatusCode, content)
	var parsedContent ZFile
	err = json.Unmarshal(content, &parsedContent)
	if err != nil {
		log.Errorf("Problem parsing zenodo's response: %v ...", err)
		return "", fmt.Errorf("problem parsing zenodo's response: %s", err)
	}

	// log.Printf("Response code %d, content: %s", resp.StatusCode, content)
	if c.Verbose {
		log.Printf("    Success (code %d (expected 201), %s Bytes). Return URI: %s", resp.StatusCode, strconv.Itoa(int(parsedContent.Filesize)), parsedContent.Links.DownloadURI)
	} else {
		log.Printf("    Success.")
	}
	return parsedContent.Links.DownloadURI, nil
}

// putMetadata adds metadata to an existing deposit and returns an error value
func putMetadata(d *Deposit, md *ZMetadata, c *Config) error {
	conf := c.Zenodo

	if md.DOI[:len(conf.Prefix)] != conf.Prefix {
		log.Errorf("Problem: DOI %s is not a zenodo DOI.", md.DOI)
		return fmt.Errorf("invalid DOI value")
	}
	id := md.DOI[len(conf.Prefix):]

	// Read metadata
	var body ZPostData
	body.Metadata = md
	mdJSON, err := json.Marshal(body)
	if err != nil {
		log.Errorf("Problem converting metadata to JSON format: %v ...", err)
		return fmt.Errorf("problem converting metadata to JSON format: %s", err)
	}
	if c.Verbose {
		log.Tracef("JSON: %s", mdJSON)
	}

	// Compile PUT request
	b := bytes.NewBuffer(mdJSON)
	uri := conf.Host + "/api/deposit/depositions/" + id // + "?access_token=" + conf.Token
	if c.Verbose {
		log.Printf("  Put metadata request to: %s", uri)
	} else {
		log.Printf("  Put metadata to zenodo")
	}

	req, err := http.NewRequest("PUT", uri, b)
	if err != nil {
		log.Errorf("Problem creating PUT request: %v ...", err)
		return fmt.Errorf("problem creating PUT request: %s", err)
	}
	req.Header.Add("Content-Type", `application/json`)
	req.Header.Add("Authorization", "Bearer "+conf.Token)
	req.Header.Add("User-Agent", "tei2zenodo (https://gitlab.gwdg.de/rg-mpg-de/tei2zenodo/)")

	// Send PUT request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("Problem sending PUT request: %v ...", err)
		return fmt.Errorf("problem sending PUT request: %s", err)
	}
	defer resp.Body.Close()

	// Handle bad responses
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Problem reading PUT response: %v ...", err)
		return fmt.Errorf("problem reading PUT response: %s", err)
	}
	if strconv.Itoa(resp.StatusCode)[:1] != "2" {
		log.Errorf("Problem reported by zenodo: %d %v. %s ...", resp.StatusCode, err, content)
		return fmt.Errorf("problem reported by zenodo: %v. %s", err, content)
	}

	// Parse response to extract filesize and download URI
	// log.Printf("Response code %d, content: %s", resp.StatusCode, content)
	var parsedContent Deposit
	err = json.Unmarshal(content, &parsedContent)
	if err != nil {
		log.Errorf("Problem parsing zenodo's response: %v ...", err)
		return fmt.Errorf("problem parsing zenodo's response: %s", err)
	}

	if c.Verbose {
		log.Printf("    Success. (code %d (expected 200)), '%s' with Id %d is %s.", resp.StatusCode, parsedContent.Title, parsedContent.RecordID, parsedContent.State)
	} else {
		log.Printf("    Success.")
	}
	return nil
}

// publish instructs zenodo to publish the deposit described in md and returns an error value
func publish(md *ZMetadata, c *Config) error {
	conf := c.Zenodo

	if md.DOI[:len(conf.Prefix)] != conf.Prefix {
		log.Errorf("Problem: DOI %s is not a zenodo DOI.", md.DOI)
		return fmt.Errorf("invalid DOI value")
	}
	id := md.DOI[len(conf.Prefix):]

	uri := conf.Host + "/api/deposit/depositions/" + id + "/actions/publish" // ?access_token=" + conf.Token
	if c.Verbose {
		log.Printf("  Post (publication) request to: %s", uri)
	} else {
		log.Printf("  Publish deposit")
	}

	// Compile POST request
	// b := bytes.NewBuffer("")
	req, err := http.NewRequest("POST", uri, nil)
	if err != nil {
		log.Errorf("Problem creating POST request: %v ...", err)
		return fmt.Errorf("problem creating POST request: %s", err)
	}
	req.Header.Add("Content-Type", `application/json`)
	req.Header.Add("Authorization", "Bearer "+conf.Token)
	req.Header.Add("User-Agent", "tei2zenodo (https://gitlab.gwdg.de/rg-mpg-de/tei2zenodo/)")

	// Send POST request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("Problem sending POST request: %v ...", err)
		return fmt.Errorf("problem sending POST request: %s", err)
	}
	defer resp.Body.Close()

	// Handle bad responses
	content, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Problem reading POST response: %v ...", err)
		return fmt.Errorf("problem reading POST response: %s", err)
	}

	if strconv.Itoa(resp.StatusCode)[:1] != "2" {
		log.Errorf("Problem reported by zenodo: %d %v. %s ...", resp.StatusCode, err, content)
		return fmt.Errorf("problem reported by zenodo: %v. %s", err, content)
	}

	// Parse response to extract filesize and download URI
	// log.Printf("Response code %d, content: %s", resp.StatusCode, content)
	var parsedContent Deposit
	err = json.Unmarshal(content, &parsedContent)
	if err != nil {
		log.Errorf("Problem parsing zenodo's response: %v ...", err)
		return fmt.Errorf("problem parsing zenodo's response: %s", err)
	}

	if c.Verbose {
		log.Printf("    Success. (code %d (expected 202)), File %s with Id %s is %s.", resp.StatusCode, parsedContent.Title, strconv.Itoa(int(parsedContent.RecordID)), parsedContent.State)
	} else {
		log.Printf("    Success. Deposit %s published.", md.DOI)
	}

	return nil
}
