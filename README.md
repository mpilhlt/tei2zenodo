# TEI to Zenodo Service

<!--
[![Go Report Card](https://goreportcard.com/badge/gitlab.gwdg.de/mpilhlt/tei2zenodo?style=flat-square)](https://goreportcard.com/report/gitlab.gwdg.de/mpilhlt/tei2zenodo)
[![Go Doc](https://img.shields.io/badge/godoc-reference-blue.svg?style=flat-square)](http://godoc.org/gitlab.gwdg.de/mpilhlt/tei2zenodo)
[![Release](https://img.shields.io/gitlab.gwdg.de/mpilhlt/tei2zenodo.svg?style=flat-square)](https://gitlab.gwdg.de/mpilhlt/tei2zenodo/releases/latest)
-->

This is the "TEI to Zenodo Webservice" software developed at the [Max Planck Institute for Legal History and Legal Theory](http://www.lhlt.mpg.de/). It is meant to provide a means to quickly push [TEI-encoded XML files](https://tei-c.org/guidelines/p5/) to [Zenodo](https://about.zenodo.org/) deposits, thereby assigning them a DOI identifier and committing them to long-term archival. The service's REST API accepts direct file uploads or webhook calls, both via POST requests. The idea is that you run the service on your infrastructure and configure a webhook in your git repository calling it. Upon being called, it looks up and retrieves the relevant TEI files from the repository and creates individual zenodo deposits for each of them.

This webservice:

- accepts and processes [github webhooks](https://developer.github.com/webhooks/) (currently of the `push` type only)
- identifies all files that have been modified in the action that triggered the webhook
- is capable of filtering by user-defined phrases that must appear in the commit message for the commit's files to be eligible
- retrieves all relevant files
- parses all TEI files and assigns values to the various [metadata fields that zenodo accepts/requires](https://developers.zenodo.org/#entities). It does this using a user-specified configuration based on (simple) [XPath](https://www.w3.org/TR/1999/REC-xpath-19991116/) expressions.
- creates a new [zenodo](https://about.zenodo.org/) deposit with a new DOI, (optionally) adds the DOI to the TEI file and then uploads it to the deposit
- is capable of uploading the deposit to zenodo and *not publishing it yet* if another user-defined phrase is present; publishes the deposit otherwise
- is capable of looking up an existing deposit (if the TEI file mentions its own zenodo DOI entry) and creating a new version of it. This new version will have a new DOI, so the software can replace the old with the new zenodo DOI before uploading the file to zenodo
- if the TEI file has changed during the process (it may now have a new DOI entry somewhere), it can push the file back to your github repository so the zenodo deposit and the github repository are in synch

When used via its file upload API endpoint (instead of the webhook listener), the service skips all the github-related parts:

- accepts a file upload of a TEI file
- parses the TEI file and assigns values to the various [metadata fields that zenodo accepts/requires](https://developers.zenodo.org/#entities). It does this using a user-specified configuration based on (simple) [XPath](https://www.w3.org/TR/1999/REC-xpath-19991116/) expressions.
- creates a new [zenodo](https://about.zenodo.org/) deposit with a new DOI, (optionally) adds the DOI to the TEI file and then uploads it to the deposit
- is capable of uploading the deposit to zenodo and *not publishing it yet* if another user-defined phrase is present; publishes the deposit otherwise
- is capable of looking up an existing deposit (if the TEI file mentions its own zenodo DOI entry) and creating a new version of it. This new version will have a new DOI, so the software deletes the old DOI and adds the new one before uploading the file to zenodo
- if the TEI file has changed during the process (it may now have a new DOI entry somewhere), the response to the POST request (i.e. the initial file upload) will be the changed file
- if the file has not changed, the response will just be the zenodo DOI that has been created

A more cursory description of how this service is used in the context of the project [The School of Salamanca](https://www.salamanca.school/) can be found in one of the projects' [blogposts](https://blog.salamanca.school/de/2020/09/23/tei-xml-to-zenodo-service-published-automatic-depositing-the-projects-tei-files-at-a-long-term-archive/).

> **Note:** Note that, since the XPath library that this service uses only supports basic XPath functions, you cannot really parse or manipulate the values via configuration settings. This means that you have to use some of zenodo's controlled vocabulary in your TEI markup! For instance, the names of licenses or if editor roles in your TEI files are expected to be compatible with zenodo. You could, for example, use the `@n`-attribute of TEI's `&lt;licence&gt;` element to hold the required string like "cc-by" or use zenodo's controlled vocabulary for contributor types (ContactPerson, DataCollector, DataCurator, DataManager, Distributor, Editor, Funder, HostingInstitution, Producer, ProjectLeader, ProjectManager, ProjectMember, RegistrationAgency, RegistrationAuthority, RelatedPerson, Researcher, ResearchGroup, RightsHolder, Supervisor, Sponsor, WorkPackageLeader, Other) to specify one of the required values in the TEI `&lt;editor&gt;`'s `@role`-attribute... (You can learn about required fields and zenodo's controlled vocabularies at [zenodo's API page](https://developers.zenodo.org/#representation).)

## Installation \& Setup

There are several ways of obtaining the software. It is not necessary to install it in a particular place, it just needs to find a configuration file (see below) and have sufficient privileges to bind to the port specified in the config. So, if you have it installed and configured, just call `tei2zenodo` (or `sudo tei2zenodo` if necessary) from a command line and that's it.

1. The default way of getting the software is downloading an asset on the [releases page](https://gitlab.gwdg.de/mpilhlt/tei2zenodo/-/releases). There are precompiled binaries zipped together with a configuration template on that page.

2. The package is maintained as a [Go](https://golang.org/) repository, so if you have Go installed, you can use it to compile and install the software in one single step: with the command `go install gitlab.gwdg.de/mpilhlt/tei2zenodo`. This will put the executable in the `$GOPATH/bin` directory, so that, on a standard Go installation, it can be found automatically from whatever directory you're in. However, you will also need to manually download and modify the [config.json](./configs/config.json.tpl?inline=false) and the [templates directory](./templates)

3. If you want to compile the source code manually yourself, you need the [Go compiler](https://golang.org/) as well. Then, after retrieving the source code, either by cloning the git repository or by using one of the "download source code" options, you can compile it in its main directory with the command `go build -ldflags "-s -w"`. You can even skip the optimization switches (`-ldflags ...`) and just say `go build`, but the longer command is the one I recommend and am using most of the time.

Interestingly, it is trivial to cross-compile code with Go, in other words, you can compile the software for many different target systems on your system. For instance, you could compile on your Windows Desktop, copy the executable over to your Linux server and run the webservice there. There is a good description of the process and of the various possible combinations of platform and architecture over at [DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-build-go-executables-for-multiple-platforms-on-ubuntu-16-04#step-4-%E2%80%94-building-executables-for-different-architectures).

## Securing the daemon and authenicating requests

Since the webservice can receive requests and webhooks from no matter where, and then relies on information contained in the request in order to "follow its nose", retrieve files and push uploads to zenodo, it is recommended that you configure some filters to limit the requests that it processes. The most basic filter is an authentication mechanism: You can configure a "secret" that is used to check incoming requests. Requests must then contain a message signature matching the checksum calculated by the service based on the secret.

Other filters are implemented in the webhook path only at the moment. They are based on specific repositories, branches or users that need to be at the origin of the webhook messages in order for them to be processed, or on a keyword that has to be present in the commit messages. This is described in more detail in the github config section, below.

## Configuration

This service is configured via a `config.json` file residing either (a) in the current working directory, (b) in the `$XDG_CONFIG_HOME/t2z` directory, (c) in the `.t2z` directory below the current user's `$HOME` directory or (d) in `/etc/t2z`. These directories are looked up in that order and the first config file found wins.

In this file, you can specify the listening port for this service, the API endpoints you want to be active (if you want to disable one, just set the value to the empty string `""`), the zenodo connection, the git context allowed to post to the webhook, if/how to write new DOI values created in the process to your TEI files, and how to parse the XML files that this service ingests into zenodo metadata fields. For an example, have a look at the [./configs/config.json.tpl](./configs/config.json.tpl) template file. Obviously, it is recommended that you make a copy of this file, adjust the settings to your needs, rename the file to `config.json` and put it in one of the places described above.

For both the github and the zenodo connections, you will need personal tokens that you can create at the respective site. Github has a [description of how to create such a token](https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line) and the [zenodo page for doing so](https://zenodo.org/account/settings/applications/tokens/new/) is really simple to understand, too. (In zenodo, your token needs "deposit:actions" and "deposit:write" privileges, in github your token needs "repo" scope.) If you use a "Secret" in your service configuration (which is recommended, see above), add the [secret](https://developer.github.com/webhooks/securing/) to your github repository, so github can calculate correct signatures for your webhook messages. If the tei2zenodo configuration has a secret set, a `signature` is required for all submissions to the File API endpoint as well - it an be either submitted either as a form field or as a query parameter, and it must contain the SHA-1 HMAC signature of the submitted file using the secret.

The configuration file is a json file that has several sections:

### Root elements

- `listenSpec` *numeric*: This allows you to specify which port the t2z daemon should be listening on. It defaults to 8081. (If you specify
   a port number below 1024, it may be necessary to rund the command with elevated privileges, e.g. `sudo tei2zenodo`.)
- `https` *string*: This allows you to take one of three approaches to tls/ssl encryption: (1) either the value is `none` (the default), in which case
  you will not have TLS/SSL encryption. Your server will listen to http requests at the port specified in `ListenSpec`. Or (2) the value is
  `letsencrypt`, in which case an automatic [Letsencrypt.org](https://letsencrypt.org/) certificate management routine will be used. This requires
  that the server runs on the standard https port 443, however, so any `ListenSpec` value is ignored (and you may need to run the program with `sudo`
  or similar). Finally, (3) you can take care of certificates yourself. Then, the service binds to the port specified in `ListenSpec` and every
  value of `Https` other than `none` and `letsencrypt` will be interpreted as colon-separated pair of paths to the private key and public
  certificate files, e.g. `/etc/ssl/private/ssl-cert-snakeoil.key:/etc/ssl/certs/t2z.pem`.
- `verbose` *boolean*: This switches between terse and verbose mode for the console output. It defaults to false, set it to true to enable more
  verbose messages.
- `apiRoot` *string*: This allows you to specify a path below which the various api endpoints can be reached. It defaults to `/api/v1`.
- `fileAPI` *string*: Subfolder of `apiRoot` where the API endpoint that receives direct file upload POST requests can be reached. Setting this
  to an empty string disables the file upload API endpoint. It defaults to `/file`.
- `webhookAPI` *string*: Subfolder of `apiRoot` where the API endpoint that receives git webhook event notifications can be reached. Setting this
  to an empty string disables the webhook API endpoint. Currently only POST requests are accepted. It defaults to `/hooks/receivers/github/events/`.
- `secret` *string*: This is a passphrase used to calculate checksums and check incoming messages against their signatures. If set, submissions require a `signature` form field or query parameter containing the SHA-1 HMAC of the payload using this secret.
- Then, there are subsections at these keys: `writeDOI`, `zenodo`, `git`, `metadata` and `log` (except where stated otherwise, all fields and values are *string* types):

### writeDOI configuration

The `writeDOI` section controls if and how the newly generated DOI will be written to the TEI file before it is committed to zenodo (and reflected back into the git repository). The service offers flexible ways of doing this, which requires setting a couple of configuration values. E.g. in order to create a new element, you have to specify not only its type, but also its parent and whether it should be created as the parent's first or last child. (As of now, all elements are referred to the TEI namespace <http://www.tei-c.org/ns/1.0>.)
The writeDOI section contains these entries:

- `mode` can be anything, but only `replace` or `add` (default) will have an effect, meaning that the new DOI value will replace the old one, or that it will just be added and no old DOI will be modified or removed (leading eventually to two different DOI values being in the TEI file, which you may or may not want). In both cases, if no DOI value is present at the specified location, the corresponding element or attribute will be created. Only if an entry is present will the choice between `add` and `replace` make a difference. If you set this to any value other than `replace` or `add`, your TEI xml file will not be modified at all.
- `parentPath` is the XPath identifying the *parent* element, of which the element holding the DOI is a child. (This defaults to "/TEI/teiHeader/fileDesc/publicationStmt".) If you want this service to modify your TEI xml file, this parent element *must* be present in your TEI file, i.e. the service will only create/modify a child of this element, but not create this parent element itself.
- `position` can be `firstChild` or `lastChild`, specifying where among the children of `parentPath` the new element should be created if it is necessary to do so. (It defaults to "lastChild".)
- `elementType` specifies which type of element is holding the DOI. The service will look up the first of `parentPath`'s children of this type. If `attributeName` is not set, the DOI will be written as this element's textual content. (It defaults to "idno".)
- `attributeName` - if set, the service will write the DOI to the respective attribute. If empty (default), the DOI will be written to a text node.
- `otherAttributes` a list of json objects made up of `attName`/`value` pairs. These allow you to set other attributes of the element holding the DOI to fixed values. It defaults to a single `"type": "DOI"` pair.
- `addChangeDesc` takes a boolean value indicating whether or not to add a `change` entry in the `revisionDesc` section of the file (if the file has changed).

Here is a complete writeDOI section, reflecting the default values. This results in new DOI values being accumulated in `/TEI/teiHeader/fileDesc/publicationStmt/idno[@type="DOI"]` elements, in chronological order, i.e. the most recent one last:

```json
"writeDOI": {
    "mode": "add",
    "parentPath": "/TEI/teiHeader/fileDesc/publicationStmt",
    "elementType": "idno",
    "otherAttributes": [
        {
            "attName": "type",
            "value": "DOI"
        }
    ],
    "position": "lastChild",
    "attributeName": "",
    "addChangeDesc": true
}
```

### zenodo service configuration

The `zenodo` section contains the access token and two settings required for testing: Zenodo offers a dedicated server for testing <https://sandbox.zenodo.org> and this provides DOIs different from the "real" zenodo DOIs at <https://zenodo.org>: The "fake" DOIs have a prefix of `10.5072/zenodo.`, the "true" DOIs have `10.5281/zenodo.`. By default, the token is empty and the service uses the sandbox version of zenodo. You should at least change the token. (Note that the sandbox and the "true" zenodo service assign different personal tokens: the one from the sandbox will not work on the "true" zenodo and vice versa.)

- the `token` key takes ... the token
- the `host` key takes the zenodo hostname
- the `prefix` key takes the DOI pattern you want to use - the DOI up to and including the full stop after "/zenodo"

Here is a complete zenodo config section:

```json
"zenodo": {
    "token": "aBcDeFgHiJkLmNoPqRsTuVwXyZ",
    "host": "https://sandbox.zenodo.org",
    "prefix": "10.5072/zenodo."
}
```

### git repository configuration

In the `git` config section, besides the obvious `host` and `token` keys, there are several keys that allow you to control what is being handled and how:

You can specify a repository in the `repo` key, the hooks of which will be processed exclusively. If hooks from other repositories come in, they will be ignored. If you leave the `repo` key empty, on the other hand, they will be processed as well. If you specify something in the `branch` key, it works as a filter in the same way; also, tei2zenodo will retrieve the files to be processed from this branch. If you leave it empty, it defaults to "main". The `hookUser` key is another filter: if you specify a value here, only hooks initiated by this github user will be processed. (In the case of "push" webhooks, the payload's `pusher.name` field is compared to this config value.)

The processing is triggered by a hook that can comprise several commits, and each of the commits can affect several files. If you specify something in the `commitTriggerPhrase` config key, only files of commits the messages of which contain the specified phrase will be processed. Leave empty to process all commits.

On the other hand, if you specify something in the `commitDontPublishPhrase` key, processing happens as normal, but the deposits of all the files affected by commits with messages that contain this phrase will not be finally published. When you log in to zenodo and click on the "Upload" button, you will see your unpublished uploads, waiting for you to inspect and finally publish them when you wish to do so...

Note that the defaults here contain dummy values that are most certainly not working, so you really should adapt this section.
Here is a complete github config section:

```json
"git": {
    "host":                    "https://api.github.com",
    "token":                   "aBcDeFgHiJkLmNoPqRsTuVwXyZ",
    "repo":                    "octocat/hello-world",
    "branch":                  "main",
    "hookUser":                "foobar",
    "commitTriggerPhrase":     "",
    "commitDontpublishPhrase": "test"
}
```

### metadata configuration

The `metadata` section is where you specify how to fill the various metadata fields that zenodo expects. You can specify pairs of metadata fields and simple XPaths at which to retrieve the text that is then going into the field like this:

```json
{
    "field": "title",
    "xPath": "//titleStmt//title[@type='main']"
},
{
    "field": "keywords",
    "xPath": "//teiHeader/profileDesc/textClass/keywords/term"
}
```

As an alternative to an XPath, you can specify an XPath expression if you want to use XPath functions. [Here](https://github.com/antchfx/xpath#supported-features) you can see which XPath patterns and functions are supported. (Note that the key's name changes from `xPath` to `xExpression`.)

```json
{
    "field": "description",
    "xExpression": "string('One of the seminal works published in the context of the project XYZ.')"
}
```

Finally, some fields are not plain text fields but rather (lists of) nested objects. This applies to the following zenodo metadata fields: `creators`, `contributors`, `thesis_supervisors`, `subjects`, `related_identifiers`, `communities`, `grants`, `locations`, `dates`.

For these, you specify the object name in the `field` value, the XPath pattern that identifies all instances of the object in your TEI file in the `xPath` value, and then, under the key `subfields`, a list of `field`/`xPath` or `field`/`xExpression` pairs. The latter are interpreted relative to the context specified in the main XPath pattern:

```json
{
    "field": "contributors",
    "xPath": "//titleStmt/editor",
    "subfields": [
        {
            "field": "name",
            "xPath": "."
        },
        {
            "field": "type",
            "xPath": "@role"
        },
        {
            "field": "orcid",
            "xPath": "@ref"
        }
    ]
}
```

For a documentation of all of zenodo's metadata fields, see [zenodo's developer page](https://developers.zenodo.org/?python#entities). For any zenodo upload, the following fields are mandatory: `title`, `creators`, `upload_type`, `publication_type` *(if `upload_type` is `publication`)*, `publication_date` *(in ISO8601 format, i.e. YYYY-MM-DD)*, `description`, `access_right`, `license` *(if `access_right` is `open` or `embargoed`)*, `embargo_date` *(if `access_right` is `embargoed`)*, `access_conditions` *(if `access_right` is `restricted`)*. Some of the fields (e.g. `contributors.type`, `access_right`, or `license`) are using a controlled vocabulary which you can look up over at [zenodo](https://developers.zenodo.org/?python#representation).

Again, remember that you probably need to meet tei2zenodo half-way by using some of zenodo's controlled vocabulary in your TEI markup.

For a full example, have a look at the [template file](./configs/config.json.tpl).

### log configuration

The `log` section contains two entries:

- a `file` setting, specifying the file that tei2zenodo should be writing its log entries to
- a `level` setting, specifying how much diagnostic informations should be logged. There are seven valid values: `Trace`, `Debug`, `Info`, `Warning`, `Error`, `Fatal` and `Panic`, ordered in decreasing verbosity. Default is "Info".

Here is a complete log config section:

```json
"log": {
    "file": "t2z.log",
    "level": "Info"
}
```

## Set up github webhook

To set up a [github webhook](https://developer.github.com/webhooks/) that triggers a zenodo upload automatically with every push, go to your repository's settings in github, click on the "Webhooks" option in the left menu, and then on the "Add Webhook" button. In the "Payload URL" field, specify your tei2zenodo daemon url, e.g. `http://123.45.123.45:8081/api/v1/hooks/receivers/github/events/`. For "Content type", select `application/json` and select to have "Just the *push* event" trigger the webhook. As soon as you save this, a ping event is sent to your server, so if your tei2zenodo daemon is listening, it should respond and give some output on its console. Also, make sure to read and understand the above with regards to the "secret" message authentication and github setup.

## API endpoints

By default, this webservice listens at the following API endpoints:

- /api/v1/file (POST, content-type: application/xml - receives a TEI file and a ?doPublish=(False|True) url query parameter)
- /api/v1/hooks/receivers/github/events/ (POST - receives what is according to github's webhook specs)

(You can change these paths in the configuration file.)

## Development

This service has been written in [Go](https://golang.org/) by [Andreas Wagner](https://orcid.org/0000-0003-1835-1653) (fedi: [@anwagnerdreas@hcommons.social](https://hcommons.social/@anwagnerdreas)). Suggestions, error reports and other feedback are welcome. You are invited to open an issue at <https://gitlab.gwdg.de/mpilhlt/tei2zenodo/-/issues>. There are some [issues that need discussion](https://gitlab.gwdg.de/mpilhlt/tei2zenodo/-/issues?label_name%5B%5D=discussion), where I would be particularly thankful for feedback.

Enjoy!

## License

The license for this software is the MIT license.
