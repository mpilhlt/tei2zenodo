module gitlab.gwdg.de/rg-mpg-de/tei2zenodo

go 1.16

require (
	github.com/antchfx/xmlquery v1.3.5
	github.com/antchfx/xpath v1.1.11
	github.com/beevik/etree v1.1.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/autotls v0.0.3
	github.com/gin-gonic/gin v1.6.3
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	github.com/stephenmuss/ginerus v0.0.0-20171108193839-50abaab78e01
	golang.org/x/oauth2 v0.0.0-20210323180902-22b0adad7558
)
