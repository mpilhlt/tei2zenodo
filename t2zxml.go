package main

import (
	"fmt"
	"io"
	"reflect"
	"regexp"
	"strings"
	"time"

	"github.com/antchfx/xmlquery"
	"github.com/antchfx/xpath"
	"github.com/beevik/etree"
)

// GetFilename extracts what can serve as filename (e.g. /TEI/@xml:id)
// Returns a string to serve as filename and an error value
func GetFilename(r io.Reader) (string, error) {
	// Parse document (in r) wih antchfx/xmlquery...
	doc, err := xmlquery.Parse(r)
	if err != nil {
		log.Errorf("Could not parse xml.")
		return "", NewError("errParse", "could not parse xml", 500, err)
	}
	t := xmlquery.FindOne(doc, `/TEI/@xml:id`)
	if n := t; t != nil {
		u := n.InnerText()
		return u + ".xml", nil
	}
	return "", nil
}

// ParseTEI reads a TEI file and parses its metadata into a ZMetadata variable.
// Returns a doi (maybe empty) and an error value.
func ParseTEI(r io.Reader, md *ZMetadata, c *Config) (string, *Error) {

	conf := c.Metadata
	var doc *xmlquery.Node

	log.Printf("--- Parse TEI file ---")

	// Parse document r wih antchfx/xmlquery...
	doc, PErr := xmlquery.Parse(r)
	if PErr != nil {
		log.Errorf("Could not parse xml.")
		return "", NewError("errParse", "could not parse xml", 500, PErr)
	}
	if doc == nil || (doc.FirstChild == nil && doc.InnerText() == "") {
		log.Errorf("    Parse returned no or empty node only.")
		return "", NewError("errNoTEIXML", "not a TEI XML file", 400, nil)
	}
	rootElement := xmlquery.FindOne(doc, `/*[1]`)
	if !(rootElement.NamespaceURI == "http://www.tei-c.org/ns/1.0" && rootElement.Data == "TEI") {
		log.Errorf("    This is not a TEI XML file. %+v", rootElement)
		return "", NewError("errNoTEIXML", "not a TEI XML file", 400, nil)
	}

	re := regexp.MustCompile(`\s+`)

	var complexTypes = []string{
		"[]main.ZCreator",
		"[]main.ZContributor",
		"[]main.ZIdentifier",
		"[]main.ZCommunity",
		"[]main.ZGrant",
		"[]main.ZSubject",
		"[]main.ZLocation",
		"[]main.ZDate",
	}

	// Iterate through the fields of the metadata struct (md)
	// and see if we have a config (conf) for them
	s := reflect.ValueOf(md).Elem()
	typeOfT := s.Type()
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		structFieldname := typeOfT.Field(i).Name
		structFieldtype := f.Type()
		jsonTag := typeOfT.Field(i).Tag.Get("json")
		var jsonFieldname string
		if idx := strings.IndexByte(jsonTag, ','); idx >= 0 {
			jsonFieldname = jsonTag[:strings.IndexByte(jsonTag, ',')]
		} else {
			jsonFieldname = jsonTag
		}
		for j := range conf.Fields {
			if conf.Fields[j].Field == jsonFieldname {
				// log.Debugf("Found config for %s.", jsonFieldname)
				// log.Debugf("It's a %s value...", structFieldtype)
				cplx := false
				for _, a := range complexTypes {
					if a == structFieldtype.String() {
						cplx = true
						break
					}
				}
				if cplx {
					confSubfields := conf.Fields[j].Subfields
					if conf.Fields[j].XPath != "" {
						xp := conf.Fields[j].XPath
						for _, l := range xmlquery.Find(doc, xp) {
							varType := structFieldtype.Elem()

							zc := reflect.Indirect(reflect.New(varType))
							for m := 0; m < zc.NumField(); m++ {
								zcf := zc.Field(m)
								// zStructFieldname := varType.Field(m).Name
								zStructFieldtype := zcf.Type()
								zJSONFieldname := varType.Field(m).Tag.Get("json")
								for m := range confSubfields {
									if confSubfields[m].Field == zJSONFieldname {
										// log.Debugf("Found config for %s.", zJSONFieldname)
										// log.Debugf("It's a %s value...", zStructFieldtype)
										if confSubfields[m].XPath != "" {
											xrpath := confSubfields[m].XPath
											var u string
											t := xmlquery.FindOne(l, xrpath)
											if n := t; t != nil {
												u = re.ReplaceAllString(n.InnerText(), " ")
												// log.Debugf("%s.%s found. Set to '%v' ...\n", structFieldname, zStructFieldname, u)
												zcf.SetString(u)
											}
										} else if confSubfields[m].XExpression != "" {

											xexpr, err := xpath.Compile(confSubfields[m].XExpression)
											if err != nil {
												log.Warnf("Erroneous XPath expression: %s ...", confSubfields[m].XExpression)
												return "", NewError("errBadConfig", fmt.Sprintf("erroneous XPath expression: %s", confSubfields[m].XExpression), 500, err)
											}
											switch zStructFieldtype.String() {
											case "string":
												var u string
												// TODO: Check for wrong types (e.g. xexpr returning int instead of string)
												u = xexpr.Evaluate(xmlquery.CreateXPathNavigator(l)).(string)
												if u != "" {
													// log.Debugf("%s.%s found. Set to '%v' ...\n", structFieldname, zStructFieldname, u)
													zcf.SetString(u)
												}

											case "[]string":
												var u []string
												// TODO: Check for wrong types (e.g. xepr returning int instead of string)
												v := xexpr.Evaluate(xmlquery.CreateXPathNavigator(l)).([]string)
												for _, n := range v {
													u = append(u, n)
													newSlice := reflect.Append(f, reflect.ValueOf(n))
													zcf.Set(newSlice)
												}
												// log.Debugf("%s.%s found. Set to '%v' ...\n", structFieldname, zStructFieldname, u)

											default:
												log.Errorf("Unknown (hardcoded?) metadata type: %s.%s ...", structFieldtype, zStructFieldtype)
												return "", NewError("errInternal", fmt.Sprintf("xml: unknown (hardcoded?) metadata type: %s.%s", structFieldtype, zStructFieldtype), 500, nil)
											}

										} else if confSubfields[m].Field == "name" || confSubfields[m].Field == "type" {
											log.Errorf("Problem with config: XPath or XExpression missing in %v ...", conf.Fields[j])
											return "", NewError("errBadConfig", fmt.Sprintf("XPath or XExpression missing in %v ...", conf.Fields[j]), 500, nil)
										}
									}
								}
							}
							switch varType.Name() {
							case "ZCreator":
								newSlice := reflect.Append(f, reflect.ValueOf(zc.Interface().(ZCreator)))
								f.Set(newSlice)
							case "ZContributor":
								newSlice := reflect.Append(f, reflect.ValueOf(zc.Interface().(ZContributor)))
								f.Set(newSlice)
							case "ZIdentifier":
								newSlice := reflect.Append(f, reflect.ValueOf(zc.Interface().(ZIdentifier)))
								f.Set(newSlice)
							case "ZCommunity":
								newSlice := reflect.Append(f, reflect.ValueOf(zc.Interface().(ZCommunity)))
								f.Set(newSlice)
							case "ZGrant":
								newSlice := reflect.Append(f, reflect.ValueOf(zc.Interface().(ZGrant)))
								f.Set(newSlice)
							case "ZSubject":
								newSlice := reflect.Append(f, reflect.ValueOf(zc.Interface().(ZSubject)))
								f.Set(newSlice)
							case "ZLocation":
								newSlice := reflect.Append(f, reflect.ValueOf(zc.Interface().(ZLocation)))
								f.Set(newSlice)
							case "ZDate":
								newSlice := reflect.Append(f, reflect.ValueOf(zc.Interface().(ZDate)))
								f.Set(newSlice)
							default:
								log.Errorf("Problem with type conversion of %s (%s)", structFieldname, varType.Name())
								return "", NewError("errInternal", fmt.Sprintf("type problem in %s [%s]", structFieldname, varType.Name()), 500, nil)

							}
						}
					} else {
						log.Errorf("Problem with config: XPath missing in %v ...", conf.Fields[j])
						return "", NewError("errBadConfig", fmt.Sprintf("XPath or XExpression missing in %v ...", conf.Fields[j]), 500, nil)
					}

				} else if conf.Fields[j].XPath != "" {
					xpath := conf.Fields[j].XPath
					switch structFieldtype.String() {
					case "string":
						var u string
						t := xmlquery.FindOne(doc, xpath)
						if n := t; t != nil {
							u = re.ReplaceAllString(n.InnerText(), " ")
							// log.Debugf("%s found. Set to '%v' ...\n", structFieldname, u)
							f.SetString(u)
						}

					case "[]string":
						var u []string
						for _, n := range xmlquery.Find(doc, xpath) {
							v := re.ReplaceAllString(n.InnerText(), " ")
							u = append(u, v)
							newSlice := reflect.Append(f, reflect.ValueOf(v))
							f.Set(newSlice)
						}
						// log.Debugf("%s found. Set to '%v' ...\n", structFieldname, u)

					default:
						log.Errorf("Unknown (hardcoded?) metadata type: %s ...", structFieldtype)
						return "", NewError("errInternal", fmt.Sprintf("xml: unknown (hardcoded?) metadata type: %s", structFieldtype), 500, nil)
					}
				} else if conf.Fields[j].XExpression != "" {
					xexpr, err := xpath.Compile(conf.Fields[j].XExpression)
					if err != nil {
						log.Errorf("Erroneous XPath expression: %s ...", conf.Fields[j].XExpression)
						return "", NewError("errBadConfig", fmt.Sprintf("erroneous XPath expression: %s", conf.Fields[j].XExpression), 500, err)
					}
					switch structFieldtype.String() {
					case "string":
						var u string
						// TODO: Check for wrong types (e.g. xepr returning int instead of string)
						u = xexpr.Evaluate(xmlquery.CreateXPathNavigator(doc)).(string)
						if u != "" {
							// log.Debugf("%s found. Set to '%v' ...\n", structFieldname, u)
							f.SetString(u)
						}

					case "[]string":
						var u []string
						// TODO: Check for wrong types (e.g. xexpr returning int instead of string)
						v := xexpr.Evaluate(xmlquery.CreateXPathNavigator(doc)).([]string)
						for _, n := range v {
							u = append(u, n)
							newSlice := reflect.Append(f, reflect.ValueOf(n))
							f.Set(newSlice)
						}
						// log.Debugf("%s found. Set to '%v' ...\n", structFieldname, u)

					default:
						log.Errorf("Unknown (hardcoded?) metadata type: %s ...", structFieldtype)
						return "", NewError("errInternal", fmt.Sprintf("xml: unknown (hardcoded?) metadata type: %s", structFieldtype), 500, nil)
					}
				} else {
					log.Errorf("Malformed config entry: %v ...", conf.Fields[j])
					return "", NewError("errBadConfig", fmt.Sprintf("XPath, XExpression or Subfields missing in %v ...", conf.Fields[j]), 500, nil)
				}
			}
		}
	}

	doi := md.DOI
	log.Printf("    TEI parsed successfully.")
	if c.Verbose {
		log.Debugf("      Title: %v", md.Title)
		if len(md.Creators) > 0 {
			log.Debugf("      Creator 1: %v", md.Creators[0])
		}
		if len(md.Contributors) > 0 {
			log.Debugf("      Contributor 1: %v", md.Contributors[0])
		}
		log.Debugf("      DOI: %s", doi)
	}

	return doi, nil
}

// MixinDOI adds a DOI idno element to the document
// Returns the string serialization of the new document and an error value
func MixinDOI(r io.Reader, doi string, c *Config) (string, error) {

	// Parse document (in r)...
	var doc etree.Document
	_, err := doc.ReadFrom(r)
	if err != nil {
		log.Errorf("Could not parse xml.")
		return "", NewError("errParse", "could not parse xml", 500, err)
	}

	// We need the parent element anyway
	// (unless we're not modifying the file at all, but in this case we shouldn't even be here)
	pElmnt := doc.FindElement(c.WriteDOI.ParentPath) // Default `/TEI/teiHeader/fileDesc/publicationStmt`
	if pElmnt == nil {
		log.Errorf("Supposed to write new DOI but XML file had no element to be found at configured parent path %s.", c.WriteDOI.ParentPath)
		return "", NewError("errParse", fmt.Sprintf("supposed to write new DOI but XML file had no element to be found at configured parent path %s", c.WriteDOI.ParentPath), 500, nil)
	}

	// Try to find the first child element of type ElementType having all necessary attributes
	var targetPathString string
	if c.WriteDOI.AttributeName != "" {
		targetPathString = c.WriteDOI.ElementType + `[@` + c.WriteDOI.AttributeName + `]`
	} else {
		targetPathString = c.WriteDOI.ElementType
	}
	for i := 0; i < len(c.WriteDOI.OtherAttributes); i++ {
		targetPathString += "[@" + c.WriteDOI.OtherAttributes[i].AttName + "='" + c.WriteDOI.OtherAttributes[i].Value + "']"
	}
	log.Debugf("Mixin DOI - search element at path %s", targetPathString)
	tElmnt := pElmnt.FindElement(targetPathString)
	if tElmnt != nil {
		log.Debugf("Mixin DOI - found element at %s", tElmnt.GetPath())
	} else {
		log.Debugf("Mixin DOI - could not find element")
	}

	// Add or replace the DOI value
	if tElmnt == nil || c.WriteDOI.Mode == "add" { // If mode = "add" or pElemnt/elementType cannot be found ...

		tElmnt := etree.NewElement(c.WriteDOI.ElementType) // ... create a new element ...
		if tElmnt == nil {
			log.Errorf("Problem creating %s element.", c.WriteDOI.ElementType)
			return "", NewError("errParse", fmt.Sprintf("problem creating %s element", c.WriteDOI.ElementType), 500, nil)
		}
		for i := 0; i < len(c.WriteDOI.OtherAttributes); i++ { // ... make sure all required attributes are there ...
			oAttr := tElmnt.CreateAttr(c.WriteDOI.OtherAttributes[i].AttName, c.WriteDOI.OtherAttributes[i].Value)
			if oAttr == nil {
				log.Errorf("Problem creating %s/@%s='%s'.", c.WriteDOI.ElementType, c.WriteDOI.OtherAttributes[i].AttName, c.WriteDOI.OtherAttributes[i].Value)
				return "", NewError("errParse", fmt.Sprintf("problem creating %s/@%s='%s'", c.WriteDOI.ElementType, c.WriteDOI.OtherAttributes[i].AttName, c.WriteDOI.OtherAttributes[i].Value), 500, nil)
			}
		}
		if c.WriteDOI.AttributeName != "" { // ... add doi as attribute ...
			tAttr := tElmnt.CreateAttr(c.WriteDOI.AttributeName, doi)
			if tAttr == nil {
				log.Errorf("Problem creating %s/@%s='%s'.", c.WriteDOI.ElementType, c.WriteDOI.AttributeName, doi)
				return "", NewError("errParse", fmt.Sprintf("problem creating %s/@%s='%s'", c.WriteDOI.ElementType, c.WriteDOI.AttributeName, doi), 500, nil)
			}
		} else { // ... or as text node ...
			tText := tElmnt.CreateText(doi)
			if tText == nil {
				log.Errorf("Problem creating text '%s' at %s.", doi, c.WriteDOI.ElementType)
				return "", NewError("errParse", fmt.Sprintf("problem creating text '%s' at %s", doi, c.WriteDOI.ElementType), 500, nil)
			}
		}
		if c.WriteDOI.Position == "firstChild" { // ... then we attach the element at the right position
			pElmnt.InsertChildAt(0, tElmnt)
		} else if c.WriteDOI.Position == "lastChild" {
			pElmnt.InsertChildAt(len(pElmnt.FindElements(`/*`))+100, tElmnt)
		} else {
			log.Errorf("Invalid value %s in WriteDOI.Position configuration (must be 'firstChild' or 'lastChild').", c.WriteDOI.Position)
			return "", NewError("errBadConfig", fmt.Sprintf("invalid value %s in WriteDOI.Position configuration (must be 'firstChild' or 'lastChild')", c.WriteDOI.Position), 500, nil)
		}

		// optionally add change element to revisionDesc
		if c.WriteDOI.AddChangeDesc {
			currentTime := time.Now()
			rdElmnt := doc.FindElement("/TEI/teiHeader/revisionDesc")
			if rdElmnt == nil {
				log.Debugf("Mixin DOI/record change - create revisionDesc")
				teiHeader := doc.FindElement("/TEI/teiHeader")
				rdElmnt = etree.NewElement("revisionDesc")
				teiHeader.InsertChildAt(len(teiHeader.FindElements(`/*`))+100, rdElmnt)
			}
			chlstElmnt := rdElmnt.FindElement("listChange")
			chgElmnt := etree.NewElement("change")
			chgElmnt.CreateAttr("when", currentTime.Format("2006-01-02"))
			chgText := chgElmnt.CreateText(fmt.Sprintf("Added DOI %s (automatically added by tei2zenodo service).", doi))
			if chgText == nil {
				log.Errorf("Problem creating text for change entry")
				return "", NewError("errParse", "problem creating text for change entry", 500, nil)
			}
			if chlstElmnt != nil {
				chlstElmnt.InsertChildAt(0, chgElmnt)
			} else {
				rdElmnt.InsertChildAt(0, chgElmnt)
			}
			log.Debugf("Mixin DOI/record change - created change entry")
		}
	} else if c.WriteDOI.Mode == "replace" { // If, on the other hand, we have mode ="replace" AND pElmnt/elementType is an element
		var oldDoi string
		if c.WriteDOI.AttributeName != "" { // if the value is in an attribute ...
			oldDoi = tElmnt.SelectAttr(c.WriteDOI.AttributeName).Value
			tAttr := tElmnt.CreateAttr(c.WriteDOI.AttributeName, doi) // ... then we only replace this attribute ...
			if tAttr == nil {
				log.Errorf("Problem creating %s/%s/@%s='%s'.", c.WriteDOI.ParentPath, c.WriteDOI.ElementType, c.WriteDOI.AttributeName, doi)
				return "", NewError("errParse", fmt.Sprintf("problem creating %s/%s/@%s='%s'", c.WriteDOI.ParentPath, c.WriteDOI.ElementType, c.WriteDOI.AttributeName, doi), 500, nil)
			}
			for i := 0; i < len(c.WriteDOI.OtherAttributes); i++ { // ... and we make sure all other required attributes are there
				oAttr := tElmnt.CreateAttr(c.WriteDOI.OtherAttributes[i].AttName, c.WriteDOI.OtherAttributes[i].Value)
				if oAttr == nil {
					log.Errorf("Problem creating %s/%s/@%s='%s'.", c.WriteDOI.ParentPath, c.WriteDOI.ElementType, c.WriteDOI.OtherAttributes[i].AttName, c.WriteDOI.OtherAttributes[i].Value)
					return "", NewError("errParse", fmt.Sprintf("problem creating %s/%s/@%s='%s'", c.WriteDOI.ParentPath, c.WriteDOI.ElementType, c.WriteDOI.OtherAttributes[i].AttName, c.WriteDOI.OtherAttributes[i].Value), 500, nil)
				}
			}
		} else {
			oldDoi = tElmnt.Text()
			tElmnt.SetText(doi) // else the value is in the text node, and we replace this text ...
			if tElmnt.Text() != doi {
				log.Errorf("Problem creating text '%s' at %s/%s.", doi, c.WriteDOI.ParentPath, c.WriteDOI.ElementType)
				return "", NewError("errParse", fmt.Sprintf("problem creating text '%s' at %s/%s", doi, c.WriteDOI.ParentPath, c.WriteDOI.ElementType), 500, nil)
			}
			for i := 0; i < len(c.WriteDOI.OtherAttributes); i++ { // ... and we make sure all required attributes are there
				oAttr := tElmnt.CreateAttr(c.WriteDOI.OtherAttributes[i].AttName, c.WriteDOI.OtherAttributes[i].Value)
				if oAttr == nil {
					log.Errorf("Problem creating %s/%s/@%s='%s'.", c.WriteDOI.ParentPath, c.WriteDOI.ElementType, c.WriteDOI.OtherAttributes[i].AttName, c.WriteDOI.OtherAttributes[i].Value)
					return "", NewError("errParse", fmt.Sprintf("problem creating %s/%s/@%s='%s'", c.WriteDOI.ParentPath, c.WriteDOI.ElementType, c.WriteDOI.OtherAttributes[i].AttName, c.WriteDOI.OtherAttributes[i].Value), 500, nil)
				}
			}
		}

		// optionally add change element to revisionDesc
		if c.WriteDOI.AddChangeDesc {
			currentTime := time.Now()
			rdElmnt := doc.FindElement("/TEI/teiHeader/revisionDesc")
			if rdElmnt == nil {
				log.Debugf("Mixin DOI/record change - create revisionDesc")
				teiHeader := doc.FindElement("/TEI/teiHeader")
				rdElmnt = etree.NewElement("revisionDesc")
				teiHeader.InsertChildAt(len(teiHeader.FindElements(`/*`))+100, rdElmnt)
			}
			chlstElmnt := rdElmnt.FindElement("listChange")
			chgElmnt := etree.NewElement("change")
			chgElmnt.CreateAttr("when", currentTime.Format("2006-01-02"))
			chgText := chgElmnt.CreateText(fmt.Sprintf("Updated DOI from %s to %s (automatically changed by tei2zenodo service).", oldDoi, doi))
			if chgText == nil {
				log.Errorf("Problem creating text for change entry")
				return "", NewError("errParse", "problem creating text for change entry", 500, nil)
			}
			if chlstElmnt != nil {
				chlstElmnt.InsertChildAt(0, chgElmnt)
			} else {
				rdElmnt.InsertChildAt(0, chgElmnt)
			}
			log.Debugf("Mixin DOI/record change - created change entry")
		}

	} else { // We have an invalid mode
		log.Errorf("Invalid value %s in WriteDOI.mode configuration (must be 'add', 'replace' or 'ignore').", c.WriteDOI.Mode)
		return "", NewError("errBadConfig", fmt.Sprintf("invalid value %s in WriteDOI.mode configuration (must be 'add', 'replace' or 'ignore')", c.WriteDOI.Mode), 500, nil)
	}

	output, err := doc.WriteToString()
	if err != nil {
		log.Errorf("Error serializing xml to string.")
		return "", NewError("errSerializing", "problem serializing XML to string", 500, err)
	} else if output == "" {
		log.Errorf("Serializing xml resulted in empty string.")
		return "", NewError("errSerializing", "serializing XML resulted in empty string", 500, err)
	}

	log.Printf("    Success.")
	return output, nil
}
